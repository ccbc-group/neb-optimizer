import numpy as np

itercount = 0
sim_fail_imgs = [5, 6]
startfac = 0.1

# this is dummy interface, producing nonsense energies
# and engrads, meant for testing if the code works
# ---------------------------------------------------------


def dummy_path_engrads(image_pvecs):
    global itercount

    dummy_energies = []
    dummy_engrads = []
    
    max_dummy_energy = ((len(image_pvecs) + 1.0) / 2.0)**2
    
    for i in range(len(image_pvecs)):
        energy = max_dummy_energy - (i+1) * (len(image_pvecs)-i+1)
        
        # to simulate a converging path, we create exponentially
        # diminishing artificial engrads
        dim_fac = startfac * np.power(0.25, itercount)
        
        engrad = dim_fac * image_pvecs[i]
        
        # to simulate failing images, we return none for energies
        # for some images for the first couple of iterations
        if itercount < 5 and i in sim_fail_imgs:
            energy = None
            engrad = np.zeros_like(engrad)
            
        # simulated CI at a reasonable position
        if i == 7:
            energy = 10000.0
        
        dummy_energies.append(energy)
        dummy_engrads.append(engrad)
        
    itercount += 1
     
    return np.array(dummy_energies), np.array(dummy_engrads)
