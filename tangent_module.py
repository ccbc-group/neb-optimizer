import numpy as np
import neb_exceptions as nex

# this module contains functions for calculating the tangent vectors
# of an NEB path. New/changed methods of tangent calculation should
# be added in this module.


def normalize(vector):
    norm = np.linalg.norm(vector)
    
    if norm != 0.0:
        vector /= norm
        
    return vector
    
    
def simple_tans(full_path_pvecs):
    tanvecs = [normalize(full_path_pvecs[i+1] - full_path_pvecs[i-1])
               for i in range(1, len(full_path_pvecs)-1)]
            
    return np.array(tanvecs)
    

def henkjon_tan_single(pos, prevpos, nextpos, en, preven, nexten):
    if en is None or preven is None or nexten is None:
        # henkelman-jonsson tangents cant be computed in this case.
        # fall back on simple tangent for this image.
        return normalize(nextpos - prevpos)
        
    tau_plus = nextpos - pos
    tau_minus = pos - prevpos

    delta_e_max = np.max([np.abs(nexten - en), np.abs(preven - en)])
    delta_e_min = np.min([np.abs(nexten - en), np.abs(preven - en)])

    if nexten > en and en > preven:
        tanvec = tau_plus

    elif nexten < en and en < preven:
        tanvec = tau_minus

    elif (nexten > en and en < preven) or (nexten < en and en > preven):
        if nexten > preven:
            tanvec = tau_plus * delta_e_max + tau_minus * delta_e_min
        else:
            tanvec = tau_minus * delta_e_max + tau_plus * delta_e_min

    else:
        # nexten == en and/or en == preven
        tanvec = tau_plus + tau_minus
        
    return normalize(tanvec)
    
    
def henkjon_tans(full_img_pvecs, full_img_energies):
    # expects len(img_pvecs) == len(img_energies)
    tanvecs = []
    
    for i in range(1, len(full_img_pvecs)-1):
        tanvec = henkjon_tan_single(full_img_pvecs[i],
                                    full_img_pvecs[i-1],
                                    full_img_pvecs[i+1],
                                    full_img_energies[i],
                                    full_img_energies[i-1],
                                    full_img_energies[i+1])
                                    
        tanvecs.append(tanvec)
        
    return np.array(tanvecs)
    
    
class SmoothedTangentEstimator:
    def __init__(self, alpha=0.8):
        self.biased_tangent = None
        self.update_count = 0
        self.alpha = alpha
        
    def reset(self):
        self.biased_tangent = None
        self.update_count = 0
        
    def update_tangent(self, current_rawtan):
        if self.biased_tangent is None:
            self.biased_tangent = np.zeros_like(current_rawtan)
    
        self.biased_tangent =\
            self.alpha * self.biased_tangent +\
            (1.0 - self.alpha) * current_rawtan
            
        self.update_count += 1
            
    def get_tangent_estimate(self):
        if self.biased_tangent is None or self.update_count == 0:
            raise nex.NEBError('Error: attempted to use smoothed tangent ' +
                               'estimator before updating with current tanvec' +
                               ' at least once!')
    
        bias_fac = 1.0 / (1.0 - self.alpha ** self.update_count)
        
        return self.biased_tangent * bias_fac
