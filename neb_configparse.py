import neb_exceptions as nex


factory_settings = {'starttraj' : None,
                    'TS_guess' : None,
                    'start_structure' : None,
                    'end_structure' : None,
                    'interface' : None,
                    'maxiter' : 500,
                    'climbing_image' : False,
                    'frozen_atom_indices' : None,
                    'sloppy_neb' : False,
                    'jobdir' : None,
                    'tempdir' : None,
                    'n_images' : 11,
                    'interp_mode' : 'internal',
                    'rot_align_mode' : 'full',
                    'do_IDPP_pass' : True,
                    'trajtest' : False,
                    'use_vark_ci' : True,
                    'k_const' : 1.0,
                    'vark_min_fac' : 0.1,
                    'use_smooth_tans' : False,
                    'AMGD_stepsize_fac' : 0.2,
                    'AMGD_max_gamma' : 0.9,
                    'max_step' : 0.05,
                    'tansmooth_alpha' : 0.8,
                    'remove_gradtrans' : True,
                    'remove_gradrot' : False,
                    'use_analytical_springpos' : False,
                    'Max_RMSF_tol' : 0.000945,
                    'Max_AbsF_tol' : 0.00189,
                    'CI_RMSF_tol' : 0.000473,
                    'CI_AbsF_tol' : 0.000945,
                    'Sloppy_Max_RMSF_tol' : 0.00945,
                    'Sloppy_Max_AbsF_tol' : 0.0189,
                    'failed_img_tol_percent' : 1.0,
                    'molpro_path' : None,
                    'gaussian_path' : None,
                    'orca_path' : None,
                    'jobname' : None,
                    'method_keywords' : None,
                    'method_line2' : None,
                    'gaussian_keywords' : None,
                    'molpro_keywords' : None,
                    'n_threads' : 1,
                    'charge' : 0,
                    'spin' : 1,
                    'memory' : 10000}


def qread(filepath):
    file = open(filepath, 'r')
    
    lines = file.readlines()
    
    file.close()
    
    return lines

   
def bettersplit(text, delim):
    tokens = []
    token = ''
    
    for char in text:
        if char not in delim:
            token += char
            
        elif token != '':
            tokens.append(token)
            
            token = ''
            
    if token != '':
        tokens.append(token)
        
    return tokens

  
def clean_eqsign(text):
    eqsign = text.index('=')
    
    if text[eqsign + 1] == ' ':
        text = text[:eqsign + 1] + text[eqsign + 2:]
        
    if text[eqsign - 1] == ' ':
        text = text[:eqsign - 1] + text[eqsign:]
        
    return text
    
    
def convert_entry(entry):
    # helper function for checking if a config entry
    # is a special kind of type (e.g. None, bool, int,...)
    # and convert them if they are
    
    if entry == 'None':
        return None
        
    elif entry == 'True':
        return True
        
    elif entry == 'False':
        return False
        
    else:
        if '.' not in entry:
            # see if it might be an int
            try:
                result = int(entry)
            except:
                pass
            else:
                return result
            
        # otherwise, see if it might be a float
        try:
            result = float(entry)
        except:
            pass
        else:
            return result
            
        # apparently, it really is a string,
        # return as-is
        return entry


def parse_inpfile(filepath, input_dict):
    lines = qread(filepath)
    
    for line in lines:
        # ignore lines with no '=' in them
        if '=' not in line:
            continue

        try:
            clean_line = clean_eqsign(line).rstrip()

            [newkey, newval] = clean_line.split('=', 1)
            
            input_dict[newkey] = convert_entry(newval)
            
        except (ValueError, IndexError):
            raise nex.NEBError("Error in neb_configparse: couldn't parse" +
                               "input file line: " + str(line))
            
    return input_dict
    
    
def load_inputfile(filepath):
    global factory_settings

    current_settings = parse_inpfile(filepath,
                                     factory_settings.copy())
    
    return current_settings
