Working prototype NEB optimizer. (Requires python 3, and a working installation of ORCA.)
NOTE: only tested with python 3.8, and the exact versions of the libraries given in requirements.txt!

INSTALLATION:
----------------------------------------------------------
1. Download or clone the neb-optimizer folder from the master branch, containing the __main__.py and the testjobs folder

----------------------------------------------------------
2. Create a virtual environment with python3 via: python3 -m venv neb-env

neb-env is the name you want to give the environment. You can name it something else, you just need to change its name in the following commands, and the testjob queue scripts.

Activate the environment using: source neb-env/bin/activate

-----------------------------------------------------------
2. b)
NOTE: In valhalla, you will probably have to use a workaround for installing the virtual environment. Instead of doing the above, do the following steps:

Create a virtual environment with python3 via: python3 -m venv neb-env --without-pip

Activate the virtual environment using: source neb-env/bin/activate

Download the get-pip.py via: curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

Install pip via: python get-pip.py

----------------------------------------------------------
3. Make sure pip is upgraded to the latest version: pip install --upgrade pip

----------------------------------------------------------
4. Install the required python modules for the neb-optimizer via: pip install -r neb-optimizer/requirements.txt

----------------------------------------------------------
5. The neb optimizer should now be ready to go. To run it, you need to set up a job directory.

A job directory contains the two end structures as .xyz files, and a neb.ini,
which contains the settings the program should use for this particular job.

In the neb.ini, you have to specify which Interface to use (Orca, Molpro or Gaussian),
as well as the command/filepath with which to start the respective program from the console.
Furthermore, you can specify there the charge and spin state of your system, as well as what theory level to use.
Reference the manual.pdf and the testjobs in the 'testjobs' folder for more info.

----------------------------------------------------------
6. Running a test job (Example: Orca job)

Make sure the virtual environment is activated.
Execute the following console command:

python neb-optimizer-master neb-optimizer-master/testjobs/orca_example/example

The first argument is the path to the NEB optimizer code folder,
the second argument is the path to the job directory.

All paths must be relative to the location where your terminal is running.

Depending on how/when/where/why you downloaded and unzipped this code,
the folder names on your system might be differ from the ones given above.

You may have to execute something like

module load [your-orca-version]

before running the command, and/or specify the path to the orca executable in the neb.ini,
of the testjob's jobdir.

The program should then start the testjob, do 10 NEB iterations, then stop. This might take a while.
Output will be printed to the console, containing relative and absolute image energies along the path.
The path will not be converged after 10 iterations, this is just a quick test.
A folder named 'results' should appear in the job directory, containing:

starttraj.xyz - the NEB starting path
currenttraj.xyz - what the NEB path currently looks like. Continuously updated while the job is running.
finaltraj.xyz - State of the NEB path at the end of the job. In this case, after only 10 iterations.
highest_en_img.xyz - structure of the highest energy image. In a converged path, a good estimate of the TS structure.
optdata.csv - Log data on how the optimization went - more important for debugging.

One can save the console output to a txt file by running the job with a command like this:

python neb-optimizer-master neb-optimizer-master/testjobs/example/orca_example > neb-optimizer-master/testjobs/orca_example/example/output.txt


If there are any questions regarding the program and how to use it,
you can reach me at bjoern.hein-janke@uni-goettingen.de
