import numpy as np
from copy import deepcopy


def do_opt_loop(NEBPath_obj,
                engrad_calc_func,
                sprgrad_calc_func,
                tanvec_calc_func,
                nebgrad_calc_func,
                step_pred_func,
                conv_signal_func,
                giveup_signal_func=None,
                failed_img_repl_func=None,
                maxiter=500,
                engrad_calc_kwargs={},
                sprgrad_calc_kwargs={},
                tanvec_calc_kwargs={},
                nebgrad_calc_kwargs={},
                step_pred_kwargs={},
                conv_signal_kwargs={},
                giveup_signal_kwargs={},
                failed_img_repl_kwargs={},
                startiter=0,
                silent_mode=False):
                
        # if the two optional functions arent provided,
        # replace them with expressions that do nothing
        if giveup_signal_func is None:
            giveup_signal_func = lambda NEBPath_obj, steps: False
            
        if failed_img_repl_func is None:
            failed_img_repl_func = lambda NEBPath_obj: NEBPath_obj

        if not silent_mode:
            print('Beginning NEB optimization loop.')
        
        return_state = 'FAILED'
        enditer = 0
        
        for i in range(startiter, maxiter):
            if not silent_mode:
                print('\nIteration no.', i+1)
                print('Updating energies and gradients.')
            
            energies, engrads = engrad_calc_func(NEBPath_obj.get_img_pvecs(),
                                                 **engrad_calc_kwargs)
                                                 
            NEBPath_obj.set_energies(energies)
            NEBPath_obj.set_engrads(engrads)
            
            if not silent_mode:
                print('Updating springforce gradients.')
                
            springgrads = sprgrad_calc_func(NEBPath_obj,
                                            **sprgrad_calc_kwargs)
            
            NEBPath_obj.set_springgrads(springgrads)
            
            if not silent_mode:
                print('Updating tangent vectors.')
                
            tanvecs = tanvec_calc_func(NEBPath_obj,
                                       **tanvec_calc_kwargs)
            
            NEBPath_obj.set_tanvecs(tanvecs)
            
            if not silent_mode:
                print('Calculating NEB gradients.')
                
            # this function also needs to take care of gradient freezing
            nebgrads = nebgrad_calc_func(NEBPath_obj,
                                         **nebgrad_calc_kwargs)
                                                 
            NEBPath_obj.set_nebgrads(nebgrads)
            
            if not silent_mode:
                print('Calculating optimization steps.')
                
            # this function also needs to take care of step freezing
            # and of doing analytical position steps for spring forces
            steps = step_pred_func(NEBPath_obj,
                                   **step_pred_kwargs)
            
            if not silent_mode:
                print('Checking for signals of convergence.')
                
            # this function also needs to take care of logging
            if conv_signal_func(NEBPath_obj,
                                steps,
                                **conv_signal_kwargs):
                return_state = 'SUCCESS'
                
                enditer = i
                
                break
                
            # this function should be used to abort the NEB if it has
            # reached an unrecoverable state. It should also print
            # a message as to why the NEB was aborted.
            if giveup_signal_func(NEBPath_obj,
                                  steps,
                                  **giveup_signal_kwargs):
                enditer = i
                
                break
            
            if not silent_mode:
                print('Applying optimization steps.')
                
            NEBPath_obj.set_img_pvecs(NEBPath_obj.get_img_pvecs() + steps)
            
            if not silent_mode:
                print('Checking for failed images.')
                
            # This function should try to replace structures
            # if their current energy is None, indicating they didn't converge,
            # indicating that their structures are nonsensical
            NEBPath_obj = failed_img_repl_func(NEBPath_obj,
                                               **failed_img_repl_kwargs)
                                               
                                                            
        else:
            enditer = maxiter
        
            if not silent_mode:
                print('\n\nWarning: the NEB did not converge, it just ' +
                      ' reached the maximum number of iterations! Be ' +
                      ' careful with the results!\n\n')
                  
        return NEBPath_obj, return_state, enditer
           

class NEBPath:
    def __init__(self,
                 start_pvec,
                 end_pvec,
                 start_energy,
                 end_energy,
                 img_pvecs,
                 labels,
                 initial_k,
                 initial_ci_index=None):
        
        self.labels = labels.copy()
        self.start_pvec = start_pvec.copy()
        self.end_pvec = end_pvec.copy()
        self.start_energy = start_energy
        self.end_energy = end_energy
        self.img_pvecs = img_pvecs.copy()
        self.energies = np.zeros(len(img_pvecs))
        self.engrads = np.zeros_like(img_pvecs)
        self.springgrads = np.zeros_like(img_pvecs)
        self.tanvecs = np.zeros_like(img_pvecs)
        self.nebgrads = np.zeros_like(img_pvecs)
        self.img_pair_ks = np.zeros(len(self.img_pvecs)+1) + initial_k
        self.ci_index = initial_ci_index
        self.misc_data = {}
        
    def n_failed_images(self):
        count = 0
        
        for energy in self.energies:
            if energy is None:
                count += 1
                
        return count
        
    def n_img_dim(self):
        return len(self.start_pvec)
        
    def n_images(self, include_ends=False):
        result = len(self.img_pvecs)
        
        if include_ends:
            result += 2
            
        return result
        
    def get_ci_index(self, include_ends=False):
        if self.ci_index is None:
            return None
    
        elif include_ends:
            return self.ci_index + 1
            
        else:
            return self.ci_index
        
    def set_ci_index(self, new_ci_index):
        self.ci_index = new_ci_index
        
    def update_ci_index(self):
        # update ci index to be the current highest energy image
    
        # replace Nones in case there are still failed images
        img_energies = [value if value is not None else -np.inf
                        for value in self.energies]
        
        self.ci_index = np.argmax(img_energies)
        
    def get_img_pair_ks(self):
        return self.img_pair_ks.copy()
        
    def set_img_pair_ks(self, new_ks):
        self.img_pair_ks = new_ks.copy()
        
    def set_img_k_const(self, kval):
        self.img_pair_ks = np.zeros(len(self.img_pvecs)+1) + kval
        
    def get_img_pvecs(self, include_ends=False):
        if include_ends:
            return np.vstack([self.start_pvec,
                              self.img_pvecs,
                              self.end_pvec])
            
        else:
            return self.img_pvecs.copy()
        
    def set_img_pvecs(self, new_img_pvecs):
        self.img_pvecs = new_img_pvecs.copy()
    
    def get_energies(self, include_ends=False):
        if include_ends:
            return np.concatenate([[self.start_energy],
                                   self.energies,
                                   [self.end_energy]])
                                   
        else:
            return self.energies.copy()
        
    def set_energies(self, new_energies):
        self.energies = new_energies.copy()
        
    def get_engrads(self):
        return self.engrads.copy()
        
    def set_engrads(self, new_engrads):
        self.engrads = new_engrads.copy()
        
    def get_springgrads(self):
        return self.springgrads.copy()
        
    def set_springgrads(self, new_springgrads):
        self.springgrads = new_springgrads.copy()
        
    def get_tanvecs(self):
        return self.tanvecs.copy()
        
    def set_tanvecs(self, new_tanvecs):
        self.tanvecs = new_tanvecs.copy()
        
    def get_nebgrads(self):
        return self.nebgrads.copy()
        
    def set_nebgrads(self, new_nebgrads):
        self.nebgrads = new_nebgrads.copy()
        
    def get_labels(self):
        return self.labels.copy()
        
    def __copy__(self):
        new_copy = NEBPath(self.start_pvec,
                           self.end_pvec,
                           self.start_energy,
                           self.end_energy,
                           self.img_pvecs,
                           self.labels)
                           
        new_copy.set_energies(self.energies)
        new_copy.set_engrads(self.engrads)
        new_copy.set_springgrads(self.springgrads)
        new_copy.set_tanvecs(self.tanvecs)
        new_copy.set_nebgrads(self.nebgrads)
        
        new_copy.misc_data = deepcopy(self.misc_data)
        
        return new_copy
