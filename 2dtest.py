import numpy as np
import matplotlib.pyplot as plt
import springforce_module as spr
import tangent_module as tan
import nebgrad_module as ngr
import step_pred_module as spm
import convsig_module as con
from neb_optimizer import NEBPath, do_opt_loop
    

def main():
    # first, we have to gather the two minima,
    # and generate a starting path via interpolation.
    
    min1 = np.array([-2.805118, 3.131312])
    min2 = np.array([3.584428, -1.848126])
    
    # generate 11 images in between the two mins
    # final length is 13
    path = interpolate_linear(min1, min2, 11)
    
    # --------------------------------------------------
    # firstly, generate an NEBPath object from these
    # interpolated 'structures'
    
    # there are cases where the energies of the minima
    # are needed, but in this simplified setup,
    # they are just a formality
    min1_en = hblau(min1)
    min2_en = hblau(min2)
    
    # set up the neb path object that will store images,
    # engrads, tanvecs etc.
    nebpath = NEBPath(start_pvec=min1,
                      end_pvec=min2,
                      start_energy=min1_en,
                      end_energy=min2_en,
                      img_pvecs=path[1:-1], # interpolate_linear outputs the ends again as well
                      labels=[],  # atomic labels dont matter in this simplified setup
                      initial_k=1.0)

    # perform the NEB optimization with maxiter = 10
    # in this simplified setup, none of the callback functions
    # need any additional arguments. They can be found in thh
    # section below.
    
    opt_nebpath, result_state, enditer =\
        do_opt_loop(NEBPath_obj=nebpath,
                    engrad_calc_func=calc_hblau_engrads,
                    sprgrad_calc_func=calc_springgrads,
                    tanvec_calc_func=calc_tanvecs,
                    nebgrad_calc_func=calc_nebgrads,
                    step_pred_func=calc_opt_steps,
                    conv_signal_func=check_conv_signals,
                    giveup_signal_func=check_giveup,
                    failed_img_repl_func=replace_failed_images,
                    maxiter=10)
                    
    print(result_state)
    print('Ended at iteration:', enditer)
    print(opt_nebpath.get_img_pvecs(include_ends=True))


# in the following section,
# the callback functions that the optimizer loop uses
# to calculate engrads, tangents, optsteps, etc.,
# as well as their respective helper functions.
# we can modify them in whatever way we want
# --------------------------------------------------


def calc_hblau_engrads(img_pvecs):
    # this function should output the images'
    # energies and energy gradients
    energies = [hblau(img) for img in img_pvecs]
    
    gradients = [hblau_grad(img) for img in img_pvecs]
    
    return np.array(energies), np.array(gradients)
    

def calc_springgrads(nebpath):
    # this function should output the images'
    # springforce gradients (not parallelized).
    # Here, we use simple springforce calculation
    # with all spring constants (k) equal 1
    full_path_pvecs = nebpath.get_img_pvecs(include_ends=True)
    
    img_pair_ks = np.ones(len(full_path_pvecs)-1)
    
    return spr.simple_springgrads(full_path_pvecs, img_pair_ks)
    
    
def calc_tanvecs(nebpath):
    # this function should output the image tangent
    # vectors. Here we use the most basic implementation
    full_path_pvecs = nebpath.get_img_pvecs(include_ends=True)
    
    return tan.simple_tans(full_path_pvecs)
    
    
def calc_nebgrads(nebpath):
    # this function is called after the previous three,
    # and is supposed to extract the new engrads, springgrads,
    # tangents from the nebpath, to calculate the nebgrad.
    # again, most basic version
    engrads = nebpath.get_engrads()
    
    springgrads = nebpath.get_springgrads()
    
    tangents = nebpath.get_tanvecs()
    
    nebgrads = ngr.calculate_nebgrads(engrads,
                                      springgrads,
                                      tangents,
                                      ci_index=None)
                                      
    return nebgrads
    
    
def calc_opt_steps(nebpath):
    # this function is called after the above,
    # and should compute the optimization step
    # for each image, based on neb gradients
    # stored in nebpath
    # In this basic implementation, things like
    # failed calculations / empty engrads dont
    # exist, which simplifies things.
    
    # first, we need to see if the AMGD predictors
    # have already been set up in the nebpath.
    # if not, this is the first iteration, and we
    # have to set them up now.
    if 'AMGD_objs' not in nebpath.misc_data:
        setup_AMGD(nebpath)
    
    # retrieve AMGD objects and neb gradients
    nebgrads = nebpath.get_nebgrads()
    AMGD_objs = nebpath.misc_data['AMGD_objs']
    
    steps = [AMGD_obj.predict(nebgrad) for nebgrad, AMGD_obj
             in zip(nebgrads, AMGD_objs)]
             
    return np.array(steps)
    
# helper function for the step predictor function above,
# which does the AMGD predictor setup in the first iteration
def setup_AMGD(nebpath):
    n_imgs = nebpath.n_images()
    ndim = nebpath.n_img_dim()
    
    AMGD_step_predictors = [spm.AMGD(ndim, stepsize_fac=0.01)
                            for i in range(n_imgs)]
    
    nebpath.misc_data['AMGD_objs'] = AMGD_step_predictors


def check_conv_signals(nebpath, steps):
    # this function should check for signals of convergence,
    # and return True if NEB convergence has been reached,
    # False otherwise.
    # It should also take care of logging information about
    # the progress of the NEB optimization, if that is desired.
    # Here, we wont do any logging, but will do the visualization
    # for the current iteration here.
    
    # some basic thresholds (a bit relaxed)
    nebgrads = nebpath.get_nebgrads()
    
    rms_f = con.NEB_RMSF(nebgrads)
    max_abs_f = con.NEB_ABSF(nebgrads)
    
    print('RMSF:', rms_f, 'Tol.', 0.00945)
    print('ABSF:', max_abs_f, 'Tol.', 0.0189)
    
    is_converged = rms_f <= 0.00945 and max_abs_f <= 0.0189
    
    # do the visualization
    plt.figure(1)
    plt.clf()
    
    draw_func(hblau, xmin=-6.0, xmax=6.0, ymin=-6.0, ymax=6.0, res=1000)
    
    full_path_pvecs = nebpath.get_img_pvecs(include_ends=True)
    
    draw_path(full_path_pvecs)
    
    plt.show()
    
    plt.pause(1)
    
    # now return result
    return is_converged
    
    
def check_giveup(nebpath, steps):
    # this function should return True if the NEB optimization
    # has reached an unrecoverable state, so it can be aborted.
    # This should be impossible in this model system, so we
    # just always return False (no giving up).
    return False
    
    
def replace_failed_images(nebpath):
    # this function should edit the nebpath
    # object to replace images whose engrad
    # calculations were failed previously,
    # meaning they have nonsensical structures.
    # This is impossible in this model system,
    # so we just always return it unchanged.
    return nebpath
    
    
# the following is the 2d 'PES' for which we want to compute
# our test NEB, with helper functions to compute
# its gradient
# --------------------------------------------------


def hblau(pvec):
    [x, y] = pvec
    
    part1 = x*x + y - 11.0
    part2 = x + y*y - 7.0
    
    return part1**2 + part2**2


def numgrad(func, x, numstep=1e-8, func_kwargs={}):
    # numerically computes the gradient for arbitrary
    # scalar function 'func'
    gradvals = []
    
    for i in range(len(x)):
        new_x = x.copy()
        new_x[i] += numstep
        val_fwd = func(new_x, **func_kwargs)
        
        new_x = x.copy()
        new_x[i] -= numstep
        val_bwd = func(new_x, **func_kwargs)
        
        grad = (val_fwd - val_bwd) / (2.0 * numstep)
        
        gradvals.append(grad)
        
    return np.array(gradvals)
    
    
def hblau_grad(pvec):
    return numgrad(hblau, pvec)
    
    
# helper functions for plotting the 'PES' and the NEB 
# and the NEB trajectory on top of it
# --------------------------------------------------
    
    
def draw_func(func, xmin, xmax, ymin, ymax, res=1000, func_kwargs={}):
    xvals = np.linspace(xmin, xmax, num=res)
    yvals = np.linspace(ymin, ymax, num=res)
    
    img = np.zeros((res, res))
    
    for yind in range(res):
        for xind in range(res):
            xval = xvals[xind]
            yval = yvals[res - yind - 1]
            
            pvec = np.array([xval, yval])
            
            img[yind, xind] = func(pvec, **func_kwargs)
            
    plt.imshow(img, extent=[xmin, xmax, ymin, ymax], cmap='prism')
    
 
def draw_path(pvecs):
    xvals = pvecs[:, 0]
    yvals = pvecs[:, 1]
    
    plt.plot(xvals, yvals, 'o-')
    
    
# miscellaneous helper functions
# ------------------------------

    
def interpolate_linear(start, end, n_inter):
    n_imgs = n_inter + 2
    
    intp_facs = np.linspace(0.0, 1.0, num=n_imgs)
    
    imgs = [start + (end - start) * fac for fac in intp_facs]
    
    return np.array(imgs)



if __name__ == '__main__':
    main()