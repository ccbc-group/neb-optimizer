#! /bin/bash -l
#SBATCH -p hound
#SBATCH -N 1   # node count
#SBATCH --ntasks-per-node=12
#SBATCH -t 2:00:00
#SBATCH --mem=30G
#SBATCH -o neb-optimizer/testjobs/molpro_example/example/batchlog.out

mkdir -p /scratch/nebtest
export TMPDIR="/scratch/nebtest"
export MOL_EXE="/opt/software/molpro2019.2/bin/molpro"
module load Molpro/molpro2019.2
module load python3.6
source /home/bheinjanke/testenv/bin/activate
python /home/bheinjanke/neb-optimizer /home/bheinjanke/neb-optimizer/testjobs/molpro_example/example > /home/bheinjanke/neb-optimizer/testjobs/molpro_example/example/output.txt

