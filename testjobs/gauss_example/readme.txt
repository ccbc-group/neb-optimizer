Example job using the Gaussian interface for EnGrad calculations.
'example' folder contains the minimum structures and the neb settings ini.
'gauss_neb.sl' was the script used to run the job on our system.
You will probably have to revise the script to get it to run on your system.
'reference_results' folder contains an example output file, which your output
file should resemble if you run this job. Since NEB calculations tend to be
time-consuming, this test job is supposed to terminate after 10 NEB iterations.
