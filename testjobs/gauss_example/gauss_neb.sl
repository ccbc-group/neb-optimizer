#! /bin/bash -l
#SBATCH -p hound
#SBATCH -N 1   # node count
#SBATCH --ntasks-per-node=12
#SBATCH -t 2:00:00
#SBATCH --mem=30G
#SBATCH -o neb-optimizer/testjobs/gauss_example/example/batchlog.out

mkdir -p /scratch/nebtest
export TMPDIR="/scratch/nebtest"
export GAUSS_EXE="g16"
module load g16_a03
#module load g16_a03_old #in case its not in the elephant queue
module load python3.6
source /home/bheinjanke/testenv/bin/activate
python /home/bheinjanke/neb-optimizer /home/bheinjanke/neb-optimizer/testjobs/gauss_example/example > /home/bheinjanke/neb-optimizer/testjobs/gauss_example/example/output.txt
