#! /bin/bash -l
#SBATCH -p hound
#SBATCH -N 1   # node count
#SBATCH --ntasks-per-node=12
#SBATCH -t 2:00:00
#SBATCH --mem=30G
#SBATCH -o neb-optimizer/testjobs/orca_example/example/batchlog.out

mkdir -p /scratch/nebtest
export TMPDIR="/scratch/nebtest"
export ORCA_EXE="/opt/software/orca_4_1_1_linux_x86-64_openmpi313/orca"
export MPI_PATH="/opt/software/openmpi/3.1.3"
module load openmpi/3.1.3
module load python3.6
source /home/bheinjanke/testenv/bin/activate
python /home/bheinjanke/neb-optimizer /home/bheinjanke/neb-optimizer/testjobs/orca_example/example > /home/bheinjanke/neb-optimizer/testjobs/orca_example/example/output.txt
