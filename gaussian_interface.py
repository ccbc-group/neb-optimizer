import os
import subprocess
import numpy as np
import file_sys_io as io


bohr = 0.529177211  # angs per bohr


# this is a wrapper function to make the gaussian inerface function
# work with the new code
# ---------------------------------------------------------


def gaussian_path_engrads(image_pvecs,
                          labels,
                          series_name,
                          workingdir,
                          gaussian,
                          method_keywords,
                          charge,
                          spin,
                          nprocs,
                          mem,
                          title):
                      
    image_data = [[labels, pvec] for pvec in image_pvecs]
    
    result = calculate_gaussian_engrads(image_data,
                                        series_name,
                                        workingdir,
                                        gaussian,
                                        method_keywords,
                                        charge,
                                        spin,
                                        nprocs,
                                        mem,
                                        title)
                                    
    energies = []
    engrads = []
    
    # reorganize results the way the new code needs them
    for item in result:
        if item is None:
            # the calculation failed to converge
            energies.append(None)
            engrads.append(np.zeros_like(image_pvecs[0]))
            
        else:
            [energy, engrad] = item
            
            energies.append(energy)
            engrads.append(engrad)
            
    return np.array(energies), np.array(engrads)


# The following is the implementation of the ORCA interface
# ---------------------------------------------------------

# Some helper functions


def bettersplit(string, delimiters):
    tokens = []
    token = ''

    for char in string:
        if char not in delimiters:
            token += char
        elif len(token) > 0:
            tokens.append(token)
            token = ''

    if len(token) > 0:
        tokens.append(token)

    return tokens


def write_gaussian_inpfile(filename, struct_data, method_keywords,
                           charge='0', spin='1', nprocs=1, mem=1000,
                           title='Title Card Required'):

    coords = struct_data[1].reshape(len(struct_data[0]), 3)
    labels = struct_data[0]

    file = open(filename, 'w')

    try:
        file.write(r'%nprocshared=' + str(nprocs) + '\n')
        file.write(r'%mem=' + str(mem) + 'mb\n')

        file.write('# force ' + method_keywords + '\n\n')

        file.write(title + '\n\n')

        file.write(str(charge) + ' ' + str(spin) + '\n')

        for label, coord in zip(labels, coords):
            line = ' ' + str(label)

            for value in coord:
                line += '\t' + repr(value)

            line += '\n'

            file.write(line)

        file.write('\n')

    finally:
        file.close()


def read_gaussian_forcefile(filename):
    file = open(filename, 'r')

    lines = file.readlines()

    file.close()

    # read in SPE.
    Energy = None

    for line in reversed(lines):
        if 'E(' in line:
            tokens = bettersplit(line, ' \t\n')

            for token in tokens:
                try:
                    val = float(token)
                except:
                    pass
                else:
                    Energy = val
                    break

    if Energy is None:
        raise ValueError('No SPE found in gaussian result file: '
                         + str(filename))

    # read in force vector
    start_ind = lines.index(' Center     Atomic                   ' +
                            'Forces (Hartrees/Bohr)\n') + 3

    stop_ind = lines.index(' -----------------------------------------' +
                           '--------------------------\n', start_ind)

    coords = []

    for i in range(start_ind, stop_ind):
        tokens = bettersplit(lines[i], ' \t\n')

        if len(tokens) != 5:
            raise ValueError('Invalid gaussian result file: ' + str(filename))

        try:
            coords += [float(tokens[2]), float(tokens[3]), float(tokens[4])]

        except:
            raise ValueError('Error while trying to read gaussian result'
                             + ' file: ' + str(filename))

    force_vec = np.array(coords)

    return Energy, force_vec / -bohr  # Eh/bohr forces to Eh/ang gradients


def calculate_gaussian_engrad(struct_data,
                              inpfile_name,
                              workingdir,
                              gaussian,
                              method_keywords,
                              charge='0',
                              spin='1',
                              nprocs=1,
                              mem=1000,
                              title='Title Card Required'):

    # assumes that 'struct_data' is a list containing a list of n string
    # atom labels, and a numpy array of 3*n length containing the atom
    # coordinates

    io.safe_create_dir(workingdir)

    os.environ['GAUSS_SCRDIR'] = str(workingdir)

    input_filename = workingdir / (inpfile_name + '.gjf')
    output_filename = workingdir / (inpfile_name + '.log')

    # make sure no output file is left over. All output files for an
    # image will have the same name, and if one stays around, it might
    # be mistaken for the result of a future calculation that actually
    # failed.
    io.safe_delete_file(output_filename, does_not_exist_ok=True)

    print('Now calculating: ' + inpfile_name)

    write_gaussian_inpfile(input_filename, struct_data, method_keywords,
                           charge, spin, nprocs, mem, title)

    subprocess.run([str(gaussian) + ' ' + str(input_filename)], shell=True)

    try:
        Energy, Gradient = read_gaussian_forcefile(output_filename)

    except:
        return None  # engrad calculation didnt converge

    else:
        return [Energy, Gradient]


def calculate_gaussian_engrads(images,
                               series_name,
                               workingdir,
                               gaussian,
                               method_keywords,
                               charge,
                               spin,
                               nprocs,
                               mem,
                               title):

    img_names = [series_name + str(i+1) for i in range(len(images))]

    return [calculate_gaussian_engrad(image,
                                      name,
                                      workingdir,
                                      gaussian,
                                      method_keywords,
                                      charge,
                                      spin,
                                      nprocs,
                                      mem,
                                      title)
            for image, name in zip(images, img_names)]
