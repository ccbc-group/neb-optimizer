import os
import subprocess
import numpy as np
import file_sys_io as io


bohr = 0.529177211  # angs per bohr


# this is a wrapper function to make the molpro inerface function
# work with the new code
# ---------------------------------------------------------


def molpro_path_engrads(image_pvecs,
                        labels,
                        series_name,
                        workingdir,
                        molpro,
                        method_keywords,
                        charge,
                        spin,
                        nprocs,
                        mem,
                        errordir=None):
                      
    image_data = [[labels, pvec] for pvec in image_pvecs]
    
    result = calculate_molpro_engrads(image_data,
                                      series_name,
                                      workingdir,
                                      molpro,
                                      method_keywords,
                                      charge,
                                      spin,
                                      nprocs,
                                      mem,
                                      errordir)
                                    
    energies = []
    engrads = []
    
    # reorganize results the way the new code needs them
    for item in result:
        if item is None:
            # the calculation failed to converge
            energies.append(None)
            engrads.append(np.zeros_like(image_pvecs[0]))
            
        else:
            [energy, engrad] = item
            
            energies.append(energy)
            engrads.append(engrad)
            
    return np.array(energies), np.array(engrads)


# The following is the implementation of the ORCA interface
# ---------------------------------------------------------

# Some helper functions


def bettersplit(string, delimiters):
    tokens = []
    token = ''

    for char in string:
        if char not in delimiters:
            token += char
        elif len(token) > 0:
            tokens.append(token)
            token = ''

    if len(token) > 0:
        tokens.append(token)

    return tokens


def write_xyz_filestring(atom_labels, coord_vec, comment=''):
    coords = coord_vec.reshape(len(atom_labels), 3)

    if comment == '':
        comment = 'coordinates from NEB optimization module'

    comment = '\n\t' + comment + '\n'

    text = '\t' + str(len(atom_labels)) + comment

    for label, coord in zip(atom_labels, coords):
        text += label

        for number in coord:
            text += '\t' + str(repr(number))

        text += '\n'

    return text


def write_molpro_inpfile(filename, struct_data, keywords,
                         charge, spin, title='Title'):

    text = '***,' + title + '\ngeometry={\n'

    text += write_xyz_filestring(struct_data[0], struct_data[1])

    text += '}\n'

    text += 'SET,CHARGE=' + str(charge) + '\n'

    text += 'SET,SPIN=' + str(spin - 1) + '\n'

    keytokens = bettersplit(keywords, ' \n\t')

    for token in keytokens:
        text += token + '\n'

    text += 'force'

    io.qwrite(filename, text)


def read_molpro_forcefile(resultfile):
    lines = io.qread(resultfile)

    if lines[-1] != ' Molpro calculation terminated\n':
        raise ValueError('Invalid molpro engrad file.')

    # find energy
    Energy = None

    for line in reversed(lines):
        if 'energy=' in line:
            tokens = bettersplit(line, ' \n\t')

            Energy = float(tokens[-1])

            break

    if Energy is None:
        raise ValueError('Invalid molpro engrad file.')

    # find forces

    startind = lines.index(' Atom          dE/dx               dE/dy'
                           + '               dE/dz\n') + 2

    stopind = lines.index('\n', startind)

    force_vals = []

    for i in range(startind, stopind):
        tokens = bettersplit(lines[i], ' \n\t')

        if len(tokens) != 4:
            raise ValueError('Invalid molpro engrad file.')

        for token in tokens[1:]:
            force_vals.append(float(token))

    force_vec = np.array(force_vals)

    return Energy, force_vec / bohr  # convert Eh/bohr to Eh/ang


def calculate_molpro_engrad(struct_data,
                            inpfile_name,
                            workingdir,
                            molpro,
                            method_keywords,
                            charge='0',
                            spin='1',
                            nprocs=1,
                            mem=1000,
                            errordir=None):

    io.safe_create_dir(workingdir)

    input_filename = workingdir / (inpfile_name + '.com')
    output_filename = workingdir / (inpfile_name + '.out')

    # make sure no output file is left over. All output files for an
    # image will have the same name, and if one stays around, it might
    # be mistaken for the result of a future calculation that actually
    # failed.
    io.safe_delete_file(output_filename, does_not_exist_ok=True)

    print('Now calculating: ' + inpfile_name)

    write_molpro_inpfile(input_filename, struct_data,
                         method_keywords, charge, spin)

    if errordir is not None:
        esdir = errordir / (inpfile_name + '.com')
        write_molpro_inpfile(esdir, struct_data, method_keywords, charge, spin)
        
    # molpro needs to be called from within the same directory that
    # the output file is in, so we must go to that directory first
    olddir = os.getcwd()
    os.chdir(workingdir)

    procmem = int(float(mem) / float(nprocs))

    """
    subprocess.call([molpro, '--no-xml-output', '--nobackup', '-n ' + str(nprocs),
                   '-s', input_filename, '-I', workingdir, '-W', workingdir,
                   '-o', output_filename])
    """
    
    subprocess.call([molpro, '--no-xml-output', '--nobackup',
                     '-n ' + str(nprocs), '-s', input_filename.name,
                     '-I', '.', '-W', '.', '-o', output_filename.name])
                   
    # now that molpro is finished, we change back to whatever
    # directory we were in before
    os.chdir(olddir)

    try:
        Energy, Gradient = read_molpro_forcefile(output_filename)

    except:
        return None  # engrad calculation didnt converge

    else:
        return [Energy, Gradient]


def calculate_molpro_engrads(images,
                             series_name,
                             workingdir,
                             molpro,
                             method_keywords,
                             charge,
                             spin,
                             nprocs,
                             mem,
                             errordir=None):

    img_names = [series_name + str(i+1) for i in range(len(images))]

    return [calculate_molpro_engrad(image,
                                    name,
                                    workingdir,
                                    molpro,
                                    method_keywords,
                                    charge,
                                    spin,
                                    nprocs,
                                    mem,
                                    errordir)
            for image, name in zip(images, img_names)]
