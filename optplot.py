import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path


def main():
    # check if the script was called with a path as argument
    # if so, just display that one
    if len(sys.argv) > 1:
        filepath = Path(sys.argv[1])

        if os.path.isdir(filepath):
            filepath /= 'optlog.csv'
        
        show_optplots(filepath)
        
    # otherwise, enter loop of requesting filepath input,
    # then displaying them
    while True:
        user_input = input('Enter filepath of log file to display. ' +
                           'Enter nothing to terminate program.\n')
                           
        if user_input == '':
            break

        filepath = Path(user_input)

        if os.path.isdir(filepath):
            filepath /= 'optlog.csv'
        
        show_optplots(filepath)
        
        print('\n')
        
        
def show_optplots(filepath):
    linetokens = load_optlog(filepath)
        
    sv_names, sv_array = get_singleval_array(linetokens)
    
    enprofiles_array = get_energy_profile_array(linetokens)
    
    gradprofiles_array = get_gradnorm_profile_array(linetokens)
    
    raw_projcoords_array = get_raw_projcoorsds_array(linetokens)
    
    rxncoords_array = get_rxncoords_array(linetokens)
    
    fig, axes = plt.subplots(nrows=2, ncols=2)

    [ax1, ax2, ax3, ax4] = axes.flatten()
    
    do_convsig_plot(ax1, sv_names, sv_array)

    do_profile_plot(ax2, enprofiles_array, rxncoords_array)

    do_projpath_plot(ax3, raw_projcoords_array)
    
    do_gnorm_plot(ax4, gradprofiles_array)

    plt.show()
        
        
def load_optlog(filepath):
    lines = qread(filepath)
    
    line_tokens = [bettersplit(line) for line in lines]
    
    return line_tokens
        
        
def get_singleval_array(token_lines):
    field_name_lists = []
    field_val_lists = []
    
    for line in token_lines:
        field_names, field_vals = find_singlevals(line)
        
        field_name_lists.append(field_names)
        field_val_lists.append(field_vals)
        
    # find the total number of columns
    fname_set = set()
    
    for fname_list in field_name_lists:
        fname_set = fname_set.union(set(fname_list))
        
    fname_set = list(fname_set)
        
    val_array = np.zeros((len(field_name_lists), len(fname_set)))
    val_array[:, :] = np.nan
    
    for i in range(len(val_array)):
        for j in range(len(fname_set)):
            name = fname_set[j]
        
            if name not in field_name_lists[i]:
                continue
            else:
                val_ind = field_name_lists[i].index(name)
                
                val_array[i, j] = field_val_lists[i][val_ind]
                
    return fname_set, val_array
    
    
def do_convsig_plot(ax, sv_names, sv_array):
    # assumes that sv_names and their corresponding columns
    # in sv_array are in the same order
    for i in range(len(sv_names)):
        values = sv_array[:, i].flatten()
        
        ax.plot(values)
        
    ax.set_xlabel('Iteration')
    ax.set_ylabel('Value')
    
    ax.grid(True, which='both')
    ax.set_yscale('log')
    
    ax.legend(sv_names)
    
    return ax

def do_profile_plot(ax,
                    enprofiles_array,
                    rxncoords_array,
                    color=(0.5, 0.0, 1.0),
                    lastiter_color=(0.5, 1.0, 0.5)):
                    
    for i in range(len(enprofiles_array)):
        energies = enprofiles_array[i]
        etas = rxncoords_array[i]

        alpha = alpha_scalefunc(i-1, len(enprofiles_array))

        c_clr = color + tuple([alpha])

        if i == len(enprofiles_array) - 1:
            ax.plot(etas, energies, 'o-', color=lastiter_color)
            
        else:
            ax.plot(etas, energies, color=c_clr)

    ax.set_xlabel('Approx. Reaction Coordinate')
    ax.set_ylabel('Delta Energy [kJ/mol]')

    ax.grid(True, which='both')

    return ax
    
    
def alpha_scalefunc(index, maxindex, starta=0.2, stopa=1.0):
    fac = (maxindex - index) / maxindex

    # fac *= fac

    alpha = stopa * (1.0 - fac) + starta * fac

    return alpha

def do_projpath_plot(ax, raw_projcoords_array, color=(0.0, 0.5, 1.0),
                     startcolor=(1.0, 0.5, 0.5, 0.25), endcolor=(0.5, 0.5, 1.0, 1.0)):
    nimgs = int((raw_projcoords_array.shape[1] / 2))
    
    start_img_xs = []
    end_img_xs = []
    start_img_ys = []
    end_img_ys = []
    
    for i in range(nimgs):
        img_xs = raw_projcoords_array[:, i*2]
        img_ys = raw_projcoords_array[:, i*2+1]
        
        start_img_xs.append(img_xs[0])
        end_img_xs.append(img_xs[-1])
        start_img_ys.append(img_ys[0])
        end_img_ys.append(img_ys[-1])
    
        plot_path(ax, img_xs, img_ys, color=color)
        
    plot_path(ax, start_img_xs, start_img_ys, color=startcolor, markerstyle='o', linestyle='-')
    plot_path(ax, end_img_xs, end_img_ys, color=endcolor, markerstyle='o', linestyle='-')
        
    ax.set_xlabel('Alpha [Angstrom]')
    ax.set_ylabel('Beta [Angstrom]')

    ax.grid(True, which='both')

    return ax
        
    
def plot_path(ax, xvals, yvals, color, markerstyle=None, linestyle=''):
    if markerstyle is None:
        ax.plot(xvals, yvals, color=color)
    
    else:
        ax.plot(xvals, yvals, color=color,
                marker=markerstyle,
                linestyle=linestyle)    

        
def do_gnorm_plot(ax, gradprofiles_array):        
    values = np.array(gradprofiles_array).T
    
    ax.imshow(values, aspect='auto')
    
    ax.set_ylabel('Image index')
    ax.set_xlabel('Iteration')
    
    return ax
    

def find_singlevals(token_line):
    # find where the multival fields start
    # the first will be 'energies'
    
    end_index = token_line.index('energies')

    # skip the first two tokens, which contain
    # the number of images
    sval_tokens = token_line[2:end_index]
    
    field_names = []
    field_vals = []
    
    for i in range(len(sval_tokens)):
        if i % 2:
            field_vals.append(float(sval_tokens[i]))
        else:
            field_names.append(sval_tokens[i])
            
    return field_names, field_vals
            
            
def find_nimgs(token_line):
    return int(token_line[1])
    
    
def get_energy_profile_array(token_lines):
    # assumes all iterations will have the same number of images
    nimgs = find_nimgs(token_lines[0])
    
    energies_array = np.zeros((len(token_lines), nimgs))
    
    for i in range(len(token_lines)):
        token_line = token_lines[i]
    
        start_index = token_line.index('energies') + 1
        stop_index = start_index + nimgs
    
        energies = np.array([float(item) for item in
                            token_line[start_index : stop_index]])
                    
        energies_array[i] = energies
        
    return energies_array
    
    
def get_rxncoords_array(token_lines):
    # assumes all iterations will have the same number of images
    nimgs = find_nimgs(token_lines[0])
    
    rxncoords_array = np.zeros((len(token_lines), nimgs))
    
    for i in range(len(token_lines)):
        token_line = token_lines[i]
    
        start_index = token_line.index('approx_rxn_crds') + 1
        stop_index = start_index + nimgs
    
        rxncoords = np.array([float(item) for item in
                              token_line[start_index : stop_index]])
                    
        rxncoords_array[i] = rxncoords
        
    return rxncoords_array
    
    
def get_gradnorm_profile_array(token_lines):
    # assumes all iterations will have the same number of images
    nimgs = find_nimgs(token_lines[0])
    
    gradnorms_array = np.zeros((len(token_lines), nimgs - 2))
            
    for i in range(len(token_lines)):
        token_line = token_lines[i]
    
        start_index = token_line.index('gradnorms') + 1
        stop_index = start_index + nimgs - 2
    
        gradnorms = np.array([float(item) for item in
                              token_line[start_index : stop_index]])
                    
        gradnorms_array[i] = gradnorms
        
    return gradnorms_array
    
    
def get_raw_projcoorsds_array(token_lines):
    # assumes all iterations will have the same number of images
    nimgs = find_nimgs(token_lines[0])
    
    projcoords_array = np.zeros((len(token_lines), nimgs * 2))
            
    for i in range(len(token_lines)):
        token_line = token_lines[i]
    
        start_index = token_line.index('projcoords') + 1
        stop_index = start_index + nimgs * 2
    
        projcoords = np.array([float(item) for item in
                               token_line[start_index : stop_index]])
                    
        projcoords_array[i] = projcoords
        
    return projcoords_array


# miscellaneous helper functions


def qread(filepath):
    file = open(filepath, 'r')
    
    lines = file.readlines()
    
    file.close()
    
    return lines
    
    
def bettersplit(text, delim=',\n'):
    tokens = []
    token = ''
    
    for char in text:
        if char not in delim:
            token += char
            
        elif token != '':
            tokens.append(token)
            
            token = ''
            
    if token != '':
        tokens.append(token)
        
    return tokens


if __name__ == '__main__':
    main()
