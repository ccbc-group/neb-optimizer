import numpy as np
from scipy.optimize import minimize


def pvec_to_crdmat(pvec):
    natoms = int(len(pvec) / 3)
    
    return pvec.reshape((natoms, 3))

   
def crdmat_to_pvec(crdmat):
    return crdmat.flatten()

   
def median_point(crdmat):
    return np.sum(crdmat, axis=0) / len(crdmat)
    
    
def center_on_median(pvec):
    crdmat = pvec_to_crdmat(pvec)

    medpoint = median_point(crdmat)
    
    # theres almost certainly a more concise way of doing this
    # in numpy, but I'm too lazy to look it up
    for i in range(len(crdmat)):
        crdmat[i] -= medpoint
        
    return crdmat_to_pvec(crdmat)


def rmsd(vec1, vec2):
    diff = vec1 - vec2
    
    return np.sqrt(np.average(diff * diff))
    
    
def double_numgrad(x, lfunc, lfunc_kwargs={}, numstep=1e-6):
    # numerically calculates gradient for arbitrary scalar function
    # x: argument vector of the scalar function
    # lfunc: the scalar function
    # lfunc_kwargs: additional arguments if lfunc requires them
    gradvec = []
    
    for i in range(len(x)):
        x_fwd = x.copy()
        
        x_fwd[i] += numstep
    
        forward = lfunc(x_fwd, **lfunc_kwargs)
        
        x_bwd = x.copy()
        
        x_bwd[i] -= numstep
        
        backward = lfunc(x_bwd, **lfunc_kwargs)
        
        grad = (forward - backward) / (2.0 * numstep)
        
        gradvec.append(grad)
        
    return np.array(gradvec)
    
    
def rotmat3d_x(angle):
    cosa = np.cos(angle)
    sina = np.sin(angle)

    row1 = np.array([1.0, 0.0, 0.0])
    row2 = np.array([0.0, cosa, -sina])
    row3 = np.array([0.0, sina, cosa])

    return np.array([row1, row2, row3])


def rotmat3d_y(angle):
    cosa = np.cos(angle)
    sina = np.sin(angle)

    row1 = np.array([cosa, 0.0, sina])
    row2 = np.array([0.0, 1.0, 0.0])
    row3 = np.array([-sina, 0.0, cosa])

    return np.array([row1, row2, row3])


def rotmat3d_z(angle):
    cosa = np.cos(angle)
    sina = np.sin(angle)

    row1 = np.array([cosa, -sina, 0.0])
    row2 = np.array([sina, cosa, 0.0])
    row3 = np.array([0.0, 0.0, 1.0])

    return np.array([row1, row2, row3])


def rotmat3d(rotvec):
    [xangle, yangle, zangle] = rotvec

    xmat = rotmat3d_x(xangle)
    ymat = rotmat3d_y(yangle)
    zmat = rotmat3d_z(zangle)

    xymat = np.matmul(ymat, xmat)

    return np.matmul(zmat, xymat)
    
    
def rotate_structure(pvec, rotvec):
    rotmat = rotmat3d(rotvec)
    
    crdmat = pvec_to_crdmat(pvec)
    
    rot_crdmat = np.matmul(rotmat, crdmat.transpose()).transpose()
    
    return crdmat_to_pvec(rot_crdmat)
    
    
def image_pair_loss(rotvec, pvec, comp_pvec):
    r_pvec = rotate_structure(pvec, rotvec)
    
    return rmsd(r_pvec, comp_pvec)
    
    
def image_path_loss(path_rotvecs, path_pvecs):
    # assumes path_pvecs.shape = (nimages, 3*natoms)
    # and path_rotvecs.shape = (nimages-1, 3)
    
    pvecs = path_pvecs.copy()
    
    for i in range(1, len(pvecs)):
        pvecs[i] = rotate_structure(pvecs[i], path_rotvecs[i-1])
    
    loss = 0.0
    
    for i in range(len(pvecs)):
        for j in range(i+1, len(pvecs)):
            loss += rmsd(pvecs[i], pvecs[j])

    return loss            
    

def align_structure_pair(pvec, comp_pvec):
    # rotationally aligns pvec to comp_pvec.
    # assumes, and only works properly, if
    # both pvecs are centered on their median points.
    rotvec = np.zeros(3)
    
    # define loss function and gradient function for
    # the optimization; loss function is rmsd
    # between the two structures.
    def lossfunc(rotvec_guess):
        return image_pair_loss(rotvec_guess, pvec, comp_pvec)
        
    def gradfunc(rotvec_guess):
        return double_numgrad(rotvec_guess, lossfunc)
    
    # it appears to work better without the bounds
    #angle_bounds = [(0.0, 2.0*np.pi) for angle in rotvec]
        
    result = minimize(fun=lossfunc,
                      x0=rotvec,
                      method='L-BFGS-B',
                      jac=gradfunc)
                      #bounds=angle_bounds)
    
    # get the optimal alignment angles               
    rotvec = result.x
    
    # apply the optimal alignment rotation and return
    return rotate_structure(pvec, rotvec)
    
    
def align_structure_sequence(pvecs):
    # rotationally aligns all structures in a sequence,
    # such that the sum of all rmsds between all possible
    # structure pairs is minimized.
    # assumes, and only works properly, if
    # all pvecs are centered on their individual median points.
    # It does not change the rotation of the first structure.
    nstructs = len(pvecs)

    if nstructs <= 1:
        return pvecs
    
    # rotvecs is supposed to have shape (nstructs-1, 3)
    # but allocate it in flattened form to make setting
    # the values' bounds easier
    rotvecs = np.zeros((nstructs-1) * 3)
    
    # define loss function and gradient function for
    # the optimization; loss function is total rmsd
    # between all possible structure pairs.
    def lossfunc(rotvecs_guess):
        rotvecs_reshaped = rotvecs_guess.reshape((nstructs-1, 3))
    
        return image_path_loss(rotvecs_reshaped, pvecs)
                               
    def gradfunc(rotvecs_guess):
        return double_numgrad(rotvecs_guess, lossfunc)
    
    # it appears to work better without the bounds
    #angle_bounds = [(0.0, 2.0*np.pi) for angle in rotvecs]
        
    result = minimize(fun=lossfunc,
                      x0=rotvecs,
                      method='L-BFGS-B',
                      jac=gradfunc)
                      #bounds=angle_bounds)
                      
    rotvecs = result.x.reshape((nstructs-1, 3))
    
    # apply the optimal alignment rotations to their
    # corresp. structures
    for i in range(1, nstructs):
        pvecs[i] = rotate_structure(pvecs[i], rotvecs[i-1])
        
    return pvecs
    
    
def align_path(path_pvecs, rot_align_mode):
    # this function combines recnetering and aligning of
    # the structures

    # first, recenter all images on their individual median points
    for i in range(len(path_pvecs)):
        path_pvecs[i] = center_on_median(path_pvecs[i])
        
    # now do the rotational alignment if desired
    if rot_align_mode is None:
        return path_pvecs
        
    elif rot_align_mode == 'full':
        path_pvecs = align_structure_sequence(path_pvecs)
        
    elif rot_align_mode == 'pairwise':
        for i in range(1, len(path_pvecs)):
            path_pvecs[i] = align_structure_pair(path_pvecs[i],
                                                 path_pvecs[i-1])
                                                     
    else:
        raise NEBError('Error in struct_aligner interpolator_module: "' +
                       str(mode) + '" not a valid rotation alignment mode.')
                       
    return path_pvecs
