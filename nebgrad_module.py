import numpy as np

# This module contains the function to calculate NEB gradients
# from engrads, springgrads, and tangent vectors. It also contains
# methods to remove rotations and translations from the NEB gradient
# vectors. Any new methods relating to that should be added in this
# module.


# Various helper functions
# --------------------------------------------------------------------------


def pvec_to_crdmat(pvec):
    natoms = int(len(pvec) / 3)
    
    return pvec.reshape((natoms, 3))

   
def crdmat_to_pvec(crdmat):
    return crdmat.flatten()
    

def project(vector, axis):
    # assumes vector, axis are 1D numpy arrays
    # assumes axis is normalized to 1
    return axis * np.dot(vector, axis)
    
    
def reject(vector, axis):
    # assumes vector, axis are 1D numpy arrays
    # assumes axis is normalized to 1
    return vector - project(vector, axis)
    
    
# Helper functions for rejecting translations from gradient vectors
# --------------------------------------------------------------------------
    
    
def reject_x_translation(pvec):
    natoms = int(len(pvec) / 3)
    
    x_unitvec = np.array([1.0, 0.0, 0.0] * natoms)
    
    x_unitvec /= np.linalg.norm(x_unitvec)
    
    return reject(pvec, x_unitvec)
    
    
def reject_y_translation(pvec):
    natoms = int(len(pvec) / 3)
    
    y_unitvec = np.array([0.0, 1.0, 0.0] * natoms)
    
    y_unitvec /= np.linalg.norm(y_unitvec)
    
    return reject(pvec, y_unitvec)
    
    
def reject_z_translation(pvec):
    natoms = int(len(pvec) / 3)
    
    z_unitvec = np.array([0.0, 0.0, 1.0] * natoms)
    
    z_unitvec /= np.linalg.norm(z_unitvec)
    
    return reject(pvec, z_unitvec)
    
    
def reject_translations(gradvec):
    gradvec = reject_x_translation(gradvec)
    gradvec = reject_y_translation(gradvec)
    gradvec = reject_z_translation(gradvec)
    
    return gradvec
    
    
# Helper functions for rejecting rotations from gradient vectors.
# --------------------------------------------------------------------------
    
    
def reject_x_rotation(deltavec, pvec):
    crdmat = pvec_to_crdmat(pvec)
    
    # synthesize the 'unit vector' for rotation around x axis
    x_unitvec = crdmat.copy()
    
    # remove x component of all atomic coordinates
    x_unitvec[:, 0] = 0.0
    
    x_axis = np.array([1.0, 0.0, 0.0])
    
    for i in range(len(x_unitvec)):
        x_unitvec[i] = np.cross(x_unitvec[i], x_axis)
        
    norm = np.linalg.norm(x_unitvec)
    
    if norm != 0.0:
        # if the norm is 0, reject won't do anything anyway
        x_unitvec /= norm
        
    # with the unit vector constructed, reject it from deltavec
    return reject(deltavec, crdmat_to_pvec(x_unitvec))


def reject_y_rotation(deltavec, pvec):
    crdmat = pvec_to_crdmat(pvec)
    
    # synthesize the 'unit vector' for rotation around y axis
    y_unitvec = crdmat.copy()
    
    # remove y component of all atomic coordinates
    y_unitvec[:, 1] = 0.0
    
    y_axis = np.array([0.0, 1.0, 0.0])
    
    for i in range(len(y_unitvec)):
        y_unitvec[i] = np.cross(y_unitvec[i], y_axis)
        
    norm = np.linalg.norm(y_unitvec)
    
    if norm != 0.0:
        # if the norm is 0, reject won't do anything anyway
        y_unitvec /= norm
    
    # with the unit vector constructed, reject it from deltavec
    return reject(deltavec, crdmat_to_pvec(y_unitvec))
                  
                  
def reject_z_rotation(deltavec, pvec):
    crdmat = pvec_to_crdmat(pvec)
    
    # synthesize the 'unit vector' for rotation around z axis
    z_unitvec = crdmat.copy()
    
    # remove z component of all atomic coordinates
    z_unitvec[:, 2] = 0.0
    
    z_axis = np.array([0.0, 0.0, 1.0])
    
    for i in range(len(z_unitvec)):
        z_unitvec[i] = np.cross(z_unitvec[i], z_axis)
    
    norm = np.linalg.norm(z_unitvec)
    
    if norm != 0.0:
        # if the norm is 0, reject won't do anything anyway
        z_unitvec /= norm
    
    # with the unit vector constructed, reject it from deltavec
    return reject(deltavec, crdmat_to_pvec(z_unitvec))

    
def reject_rotations(gradvec, struct_pvec):
    gradvec = reject_x_rotation(gradvec, struct_pvec)
    gradvec = reject_y_rotation(gradvec, struct_pvec)
    gradvec = reject_z_rotation(gradvec, struct_pvec)
    
    return gradvec
    
    
# Helper functions for freezing atom indices
# --------------------------------------------------------------------------

def freeze_atom_indices(vector, atom_indices):
    # this function takes in a vector of length (3*natoms),
    # it can be a gradient or step vector, and it zeros
    # all entries corresponding to the given atomic indices.
    # Note, atom 1 would have index 0!
    matshape_vector = pvec_to_crdmat(vector)
    
    for index in atom_indices:
        matshape_vector[index] = 0.0
        
    edited_vector = crdmat_to_pvec(matshape_vector)
    
    return edited_vector
    
    
# Functions for calculating and sanitizing the NEB gradients
# --------------------------------------------------------------------------


def sanitize_stepvecs(vectors,
                      reject_transl=True,
                      reject_rot=False):
    
    if reject_transl:
        for i in range(len(vectors)):
            vectors[i] = reject_translations(vectors[i])
            
    if reject_rot:
        for i in range(len(vectors)):
            vectors[i] = reject_rotations(vectors[i])
            
    return vectors

    
def calculate_nebgrads(engrads,
                       sprgrads,
                       tanvecs,
                       ci_index=None):
                  
    orth_engrads = [reject(engrad, tanvec) for engrad, tanvec
                    in zip(engrads, tanvecs)]
                    
    par_sprgrads = [project(sprgrad, tanvec) for sprgrad, tanvec
                    in zip(sprgrads, tanvecs)]
                    
    nebgrads = [orth_en + par_spr for orth_en, par_spr
                in zip(orth_engrads, par_sprgrads)]
    
    # replace the nebgrad at ci_index with the climbing image gradient
    if ci_index is not None:
        ci_par_engrad = project(engrads[ci_index], tanvecs[ci_index])
        
        nebgrads[ci_index] = orth_engrads[ci_index] - ci_par_engrad
            
    return np.array(nebgrads)
    
    
"""
def calculate_reduced_nebgrads(engrads,
                               tanvecs,
                               ci_index=None):
                               
    nebgrads = [reject(engrad, tanvec) for engrad, tanvec
                in zip(engrads, tanvecs)]
    
    # replace the nebgrad at ci_index with the climbing image gradient
    if ci_index is not None:
        ci_par_engrad = project(engrads[ci_index], tanvecs[ci_index])
        
        nebgrads[ci_index] = orth_engrads[ci_index] - ci_par_engrad
            
    return np.array(nebgrads)
"""
