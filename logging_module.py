import numpy as np


def qappend(filepath, lines):
    file = open(filepath, 'a')
    
    file.writelines(lines)
    
    file.close()
    
    
def normalize(vector):
    norm = np.linalg.norm(vector)
    
    if norm != 0.0:
        vector /= norm
        
    return vector
    
    
def project_2d(nebpath_imgs):
    # first 'coordinate' is distance along a line connecting
    # the the ends
    axvec = normalize(nebpath_imgs[-1] - nebpath_imgs[0])
    
    first_coords = [np.dot(pvec, axvec) for pvec in nebpath_imgs]
    
    remainders = [pvec - fcrd * axvec for pvec, fcrd in
                  zip(nebpath_imgs, first_coords)]
                  
    # second 'coordinate' is the norm off the remainder,
    # the distance between the image and its projected
    # position on the axis above
    second_coords = [np.linalg.norm(rem) for rem in remainders]
    
    # assemble np array
    
    return np.array([[crd1, crd2] for crd1, crd2 in
                    zip(first_coords, second_coords)])


class NEBLogger:
    def __init__(self, logfile_path):
        # clear the file in case it already exists
        file = open(logfile_path, 'w')
        file.close()
    
        self.logfile_path = logfile_path
        self.prev_nebpath_obj = None
        
    def write_to_log(self, nebpath_obj, conv_sig_dict):
        path_pvecs = nebpath_obj.get_img_pvecs(include_ends=True)
        path_energies = nebpath_obj.get_energies(include_ends=True)
        path_gradnorms = [np.linalg.norm(grad) for grad in
                          nebpath_obj.get_nebgrads()]
                          
        n_total_imgs = len(path_pvecs)
        
        # first field in a line is the (currrent) number of images    
        newline = 'nimgs,' + str(n_total_imgs) + ','
        
        # add the convergence signal values
        # order: name1,val1,name2,val2,...
        for key, val in conv_sig_dict.items():
            newline += str(key) + ',' + str(val) + ','
            
        # add fields for energies and gradnorms
        newline += 'energies,'
        
        for energy in path_energies:
            newline += str(energy) + ','
            
        newline += 'gradnorms,'
        
        for norm in path_gradnorms:
            newline += str(norm) + ','
            
        # add fields for the 2d path projection
        coords_2d = project_2d(path_pvecs.copy())
        
        newline += 'projcoords,'
        
        # order: img1_crd1,img1_crd2,img2_crd1,img2_crd2,...
        for crd_pair in coords_2d:
            newline += str(crd_pair[0]) + ',' + str(crd_pair[1]) + ','
            
        # add fields for the approx. rxn. coordinates
        rxn_crds = approx_rxn_coordinates(path_pvecs)
        
        newline += 'approx_rxn_crds,'
        
        for val in rxn_crds:
            newline += str(val) + ','
            
        # this is the end of the line, remove last comma
        newline = newline[:-1] + '\n'
        
        # write to log
        qappend(self.logfile_path, [newline])


def approx_rxn_coordinates(full_path_pvecs):
    total_dist = 0.0
    dists = [0.0]
    
    for i in range(1, len(full_path_pvecs)):
        delta_pos = full_path_pvecs[i] - full_path_pvecs[i-1]
    
        total_dist += np.linalg.norm(delta_pos)
        
        dists.append(total_dist)
        
    dists = np.array(dists)
    
    dists /= dists[-1]
    
    return dists