import numpy as np
import file_sys_io as io

import subprocess

bohr = 0.529177211  # angs per bohr


# this is a wrapper function to make the orca inerface function
# work with the new code
# ---------------------------------------------------------


def orca_path_engrads(image_pvecs,
                      labels,
                      series_name,
                      workingdir,
                      orca,
                      method_keywords,
                      charge,
                      spin,
                      npal=None,
                      method_line2=None):
                      
    image_data = [[labels, pvec] for pvec in image_pvecs]
    
    result = calculate_orca_engrads(image_data,
                                    series_name,
                                    workingdir,
                                    orca,
                                    method_keywords,
                                    charge,
                                    spin,
                                    npal,
                                    method_line2)
                                    
    energies = []
    engrads = []

    # reorganize results the way the new code needs them
    for item in result:
        if item is None:
            # the calculation failed to converge
            energies.append(None)
            engrads.append(np.zeros_like(image_pvecs[0]))
            
        else:
            [energy, engrad] = item
            
            energies.append(energy)
            engrads.append(engrad)
            
    return np.array(energies), np.array(engrads)


# The following is the implementation of the ORCA interface
# ---------------------------------------------------------

# Some helper functions


def bettersplit(string, delimiters):
    tokens = []
    token = ''

    for char in string:
        if char not in delimiters:
            token += char
        elif len(token) > 0:
            tokens.append(token)
            token = ''

    if len(token) > 0:
        tokens.append(token)

    return tokens


def reverse_bettersplit(string, notdelims):
    tokens = []
    token = ''

    for char in string:
        if char in notdelims:
            token += char
        elif len(token) > 0:
            tokens.append(token)
            token = ''

    if len(token) > 0:
        tokens.append(token)

    return tokens


def sanitize_mkwords(method_keywords):
    mk_tokens = bettersplit(method_keywords.lower(), ' \t')

    if 'noautostart' not in mk_tokens:
        method_keywords += ' NoAutoStart'

    if 'engrad' not in mk_tokens:
        method_keywords += ' EnGrad'

    if 'angs' not in mk_tokens:
        method_keywords += ' Angs'

    if method_keywords[0] != '!':
        method_keywords = '!' + method_keywords

    return method_keywords


def write_orca_inpfile(filename, struct_data, method_keywords,
                       charge='0', spin='1', npal=None, method_line2=None):
    file = open(filename, 'w')
    coords = struct_data[1].reshape(len(struct_data[0]), 3)

    file.write(sanitize_mkwords(method_keywords) + '\n\n')
    
    if npal is not None:
        file.write('%PAL NPROCS ' + str(npal) + ' END\n\n')
        
    if method_line2 is not None:
        file.write(method_line2 + '\n\n')

    file.write('* xyz ' + str(charge) + ' ' + str(spin) + '\n')

    for label, coord in zip(struct_data[0], coords):
        file.write('\t' + label)

        for number in coord:
            file.write('\t' + str(repr(number)))

        file.write('\n')

    file.write('*')

    file.close()


def read_orca_engrad(filename):
    file = open(filename)
    lines = file.readlines()
    file.close()

    # Find energy
    index = lines.index("# The current total energy in Eh\n") + 2
    Energy = float(lines[index])

    # Find gradient energy
    index_start = lines.index("# The current gradient in Eh/bohr\n") + 2
    index_end = lines.index("# The atomic numbers and current" +
                            " coordinates in Bohr\n") - 1

    grad_vals = [float(line) for line in lines[index_start : index_end]]
    grad_vec = np.array(grad_vals)

    return Energy, grad_vec


def calculate_orca_engrad(struct_data,
                          inpfile_name,
                          workingdir,
                          orca,
                          method_keywords,
                          charge='0',
                          spin='1',
                          npal=None,
                          method_line2=None):
    # assumes that 'struct_data' is a list containing
    # a list of n string atom labels,
    # and a numpy array of 3*n length containing the atom coordinates
    
    output_file = None

    try:
        input_filename = workingdir / (inpfile_name + '.inp')
        engrad_filename = workingdir / (inpfile_name + '.engrad')
        output_filename = workingdir / (inpfile_name + '.out')

        # make sure no .engrad file is left over. All engrad files for an
        # image will have the same name, and if one stays around, it might
        # be mistaken for the result of a future calculation that actually
        # failed.
        io.safe_delete_file(engrad_filename, does_not_exist_ok=True)
        io.safe_delete_file(output_filename, does_not_exist_ok=True)

        print('Now calculating: ' + inpfile_name)

        write_orca_inpfile(input_filename, struct_data,
                           method_keywords, charge, spin,
                           npal=npal, method_line2=method_line2)

        output_file = open(output_filename, 'w')

        subprocess.run([orca, input_filename], stdout=output_file)

    finally:
        if output_file is not None:
            output_file.close()

    try:
        V, grad_V = read_orca_engrad(engrad_filename)

    except:
        return None  # engrad calculation didnt converge

    else:
        return [V, grad_V / bohr]  # convert engrad to Eh/Ang


def calculate_orca_engrads(images,
                           series_name,
                           workingdir,
                           orca,
                           method_keywords,
                           charge,
                           spin,
                           npal=None,
                           method_line2=None):

    img_names = [series_name + str(i+1) for i in range(len(images))]

    return [calculate_orca_engrad(image,
                                  name,
                                  workingdir,
                                  orca,
                                  method_keywords,
                                  charge,
                                  spin,
                                  npal=npal,
                                  method_line2=method_line2)
            for image, name in zip(images, img_names)]
