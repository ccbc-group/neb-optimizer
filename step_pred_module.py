import numpy as np
from scipy.optimize import LbfgsInvHessProduct


def enforce_maxstep(stepvec, max_step_norm):
    norm = np.linalg.norm(stepvec)
    
    if norm > max_step_norm:
        stepvec /= norm
        
        stepvec *= max_step_norm
        
    return stepvec
    
    
def enforce_maxsteps(stepvecs, max_step_norm):
    # this function is meant to check the norm of a series
    # of step vectors, and scale them down to meet the
    # maximum step norm if they exceed it
    for i in range(len(stepvecs)):
        stepvecs[i] = enforce_maxstep(stepvecs[i], max_step_norm)
        
    return stepvecs
    
    
class AMGD:
    def __init__(self, ndim, stepsize_fac=0.2, max_gamma=0.9, max_step=None):
        self.prev_step = np.zeros(ndim)
        self.stepsize_fac = stepsize_fac
        self.max_gamma = max_gamma
        self.max_step = max_step
        
    def reset(self):
        self.prev_step = np.zeros_like(self.prev_step)
        
    def replace_last_step(self, new_last_step):
        self.prev_step = new_last_step
        
    def predict(self, gradvec):
        prev_stepnorm = np.linalg.norm(self.prev_step)
        
        if prev_stepnorm == 0.0:
            # It's the first iteration, just do gradient descent
            step = -self.stepsize_fac * gradvec
        
        else:
            # do full step calculation
            gradnorm = np.linalg.norm(gradvec)
            prev_stepnorm = np.linalg.norm(self.prev_step)
            
            angle_denom = gradnorm * prev_stepnorm
            
            # determine r value
            if angle_denom == 0.0:
                angle_rval = 0.5
                
            else:
                angle = np.dot(gradvec, self.prev_step) / angle_denom
                
                angle_rval = 0.5 + 0.5 * angle
                
            angle_rval = min(angle_rval, 1.0)
            angle_rval = max(1.0 - self.max_gamma, angle_rval)
            
            # compute optimization step
            step = (1.0 - angle_rval) * self.prev_step - self.stepsize_fac * gradvec
            
        # apply max step norm, if option is chosen
        if self.max_step is not None:
            step = enforce_maxstep(step, self.max_step)
        
        # save step copy for next iteration and return
        self.prev_step = step.copy()
        
        return step
        
        
class InvLBFGS:
    def __init__(self, ncorr=10):
        self.ncorr = ncorr
        self.sk = []
        self.yk = []
        
    def update(self, laststep, last_delta_grad):
        self.sk.append(laststep)
        
        while len(self.sk) > self.ncorr:
            self.sk = self.sk[1:]
            
        self.yk.append(last_delta_grad)
        
        while len(self.yk) > self.ncorr:
            self.yk = self.yk[1:]
            
    def calc_invhess_dotprod(self, vector):
        invhess = LbfgsInvHessProduct(self.sk, self.yk)
        
        return invhess.dot(vector)