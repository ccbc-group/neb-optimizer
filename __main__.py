import neb_optimizer as nopt
import neb_exceptions as nex
import path_interpolator_module as pim
import neb_configparse as conf
import file_sys_io as io
import nebgrad_module as ngm
import springforce_module as sfm
import step_pred_module as spm
import tangent_module as tgm
import failed_img_recalculator as fir
import convsig_module as csm
import orca_interface as orc
import molpro_interface as mol
import gaussian_interface as gss
import dummy_interface as dmm
import logging_module as lgm
import struct_aligner_module as sam

from pathlib import Path
import numpy as np
import codecs
import time
import sys
import os
from datetime import datetime, timedelta


Hartree_in_kJmol = 2625.49963948


# the following function is what's being executing upon starting
# the program in the terminal
# --------------------------------------------------------------

def main():
    tempdir = None
    workdir = None

    try:
        # gather user-given data
        # -------------------------
        
        if len(sys.argv) < 2:
            # user forgot to provide the input file path in the command line
            print('Error: no input file given in program call. Please ' +
                  ' check the manual on how to use this program.')
                  
            return
            
        # get path to the input file(s) from the user
        inpfile_path = Path(sys.argv[1])
        
        # make sure to get the full path
        inpfile_path = get_full_path(inpfile_path)
        
        conf_dict = process_input_path(inpfile_path)
        
        # if no jobname is given, set it to be the name of the input file
        if conf_dict['jobname'] is None:
            conf_dict['jobname'] = inpfile_path.stem
            
        # find/generate workdir, where the results should be stored
        workdir = find_workdir(conf_dict, inpfile_path)
        
        # process any additional arguments given in the command line
        if len(sys.argv) > 2:
            conf_dict = process_moreargs(conf_dict, workdir, sys.argv[2:])
        
        # find/generate tempdir, where temporary files should be stored
        tempdir = find_tempdir(conf_dict)

        # find/generate the starting path
        labels, starting_path = produce_starting_path(conf_dict)
        
        # save the starting path into the workdir
        io.write_xyz_traj(labels, starting_path, workdir / 'starttraj.xyz')
        
        # if trajtest is selected, only the starting should be printed.
        # in that case, we're already done.
        if conf_dict['trajtest'] is True:
            io.safe_delete_dir(tempdir)
            return
        
        # generate NEBPath object
        # -----------------------
        # first, set up our energy and gradient calculation function
        engrfunc, engrfunc_kwargs = gather_engrfunc(labels,
                                                    conf_dict,
                                                    tempdir)
        
        # now we can generate the NEBPath object, which will hold the
        # relevant data for the neb path, like structures, labels,
        # tangent vectors, neb gradients etc.
        nebpath = setup_NEBPath_obj(labels,
                                    starting_path,
                                    engrfunc,
                                    engrfunc_kwargs,
                                    conf_dict)
        
        # set up the logger for the progress of the NEB optimization
        logfile_path = workdir / 'optlog.csv'
        
        logger = lgm.NEBLogger(logfile_path)
        
        nebpath.misc_data['logger'] = logger
        
        # do the actual NEB optimization
        # ------------------------------
        # the additional arguments required by all the callback functions
        # (except the engrad function) are the same - the conf_dict
        func_kwargs = {'conf_dict' : conf_dict}
        
        # this is used to keep track of how many NEB iterations were
        # performed in total between the different stages of the NEB
        # optimization, to make sure it does not exceed the maximum
        # number of iterations
        n_total_iterations = 0
        
        # deactivate CI for now, but remember what the user
        # set it to for later
        ci_user_setting = conf_dict['climbing_image']
        conf_dict['climbing_image'] = False
        
        conf_dict['start_time'] = time.time()
        conf_dict['curtraj_path'] = workdir / 'currenttraj.xyz'
        
        # first stage: NEB optimization with sloppy NEB convergence thresholds
        nebpath, return_state, enditer =\
            nopt.do_opt_loop(nebpath,
                             engrfunc,
                             calc_springgrads,
                             calc_tanvecs,
                             calc_nebgrads,
                             predict_AMGD_steps,
                             conv_checker_func_sloppy, ##
                             giveup_signal_func,
                             failed_image_replacer_func,
                             maxiter=conf_dict['maxiter'],
                             engrad_calc_kwargs=engrfunc_kwargs,
                             sprgrad_calc_kwargs=func_kwargs,
                             tanvec_calc_kwargs=func_kwargs,
                             nebgrad_calc_kwargs=func_kwargs,
                             step_pred_kwargs=func_kwargs,
                             conv_signal_kwargs=func_kwargs,
                             giveup_signal_kwargs=func_kwargs,
                             failed_img_repl_kwargs=func_kwargs,
                             startiter=0)
                             
        n_total_iterations += enditer
                             
        if (conf_dict['sloppy_neb'] is True or
            n_total_iterations >= conf_dict['maxiter']):
            # if sloppy NEB is active, or the iteration budget is
            # used up, we're already done now.  
            do_end_of_opt_printout(nebpath, conf_dict, workdir)
            
            return
                          
        # if the previous stage of optimization didn't converge
        # for some reason other than running out of iterations
        # (i.e. it crashed), we cant continue and must raise
        # an error
        if return_state != 'SUCCESS':
            raise nex.NEBError('Error: something went wrong during sloppy ' +
                               'NEB optimization stage. Aborting...')
        
        # if the user selected climbing image, we now need to
        # do the rest of the optimization with CI active.
        if ci_user_setting is True:
            print('\nInitial stage of NEB optimization complete. ' +
                  'Activating climbing image.\n')
            
            # activate CI. The relevant functions will see
            # this in the conf_dict
            conf_dict['climbing_image'] = ci_user_setting
            
            # set ci index to the current highest energy image
            nebpath.update_ci_index()
            
            # with the remainder of the iteration budget,
            # perform an NEB-CI optimization, with the
            # appropriate convergence checker function.
            # the rest stays the same
            nebpath, return_state, enditer =\
                nopt.do_opt_loop(nebpath,
                                 engrfunc,
                                 calc_springgrads,
                                 calc_tanvecs,
                                 calc_nebgrads,
                                 predict_AMGD_steps,
                                 conv_checker_func_ci, ##
                                 giveup_signal_func,
                                 failed_image_replacer_func,
                                 maxiter=conf_dict['maxiter'],
                                 engrad_calc_kwargs=engrfunc_kwargs,
                                 sprgrad_calc_kwargs=func_kwargs,
                                 tanvec_calc_kwargs=func_kwargs,
                                 nebgrad_calc_kwargs=func_kwargs,
                                 step_pred_kwargs=func_kwargs,
                                 conv_signal_kwargs=func_kwargs,
                                 giveup_signal_kwargs=func_kwargs,
                                 failed_img_repl_kwargs=func_kwargs,
                                 startiter=n_total_iterations)
                                 
                                 
        elif n_total_iterations < conf_dict['maxiter']:
            print('\nInitial stage of NEB optimization complete. ' +
                  'Continueing with main optimization stage.\n')
              
            # if the user didn't select CI, we now have to do
            # a regular optimization towards the full convergence
            # thresholds.
            nebpath, return_state, enditer =\
                nopt.do_opt_loop(nebpath,
                                 engrfunc,
                                 calc_springgrads,
                                 calc_tanvecs,
                                 calc_nebgrads,
                                 predict_AMGD_steps,
                                 conv_checker_func_nosloppy_noci, ##
                                 giveup_signal_func,
                                 failed_image_replacer_func,
                                 maxiter=conf_dict['maxiter'],
                                 engrad_calc_kwargs=engrfunc_kwargs,
                                 sprgrad_calc_kwargs=func_kwargs,
                                 tanvec_calc_kwargs=func_kwargs,
                                 nebgrad_calc_kwargs=func_kwargs,
                                 step_pred_kwargs=func_kwargs,
                                 conv_signal_kwargs=func_kwargs,
                                 giveup_signal_kwargs=func_kwargs,
                                 failed_img_repl_kwargs=func_kwargs,
                                 startiter=n_total_iterations)
                                 
        # whatever the user had selected is now finished.
        do_end_of_opt_printout(nebpath, conf_dict, workdir)
        
        return

    except nex.EngradError:
        # if the ends fail to converge, there's probably something
        # wrong with the way the user set up the engrad calculations.
        # copy over the calculation results so they can find out
        # what's wrong.
        if tempdir is not None:
            io.move_dir(tempdir, workdir)
            
            print('Contents of tempdir were copied to job directory.')
        
        raise
        
    finally:
        if tempdir is not None:
            print('Temporary files deleted.')
            
            # remove tempdir if it wasn't removed already
            io.safe_delete_dir(tempdir, does_not_exist_ok=True)
            
            
def do_end_of_opt_printout(nebpath, conf_dict, workdir):
    # do final printout
    print('\nFinal stats of the NEB path:\n')
    
    do_iter_printout(nebpath, conf_dict)
    
    # print final trajectory
    final_path = nebpath.get_img_pvecs(include_ends=True)
    final_energies = nebpath.get_energies(include_ends=True)
    labels = nebpath.get_labels()
    finaltraj_filepath = workdir / 'finaltraj.xyz'
    
    io.write_xyz_traj(labels,
                      final_path,
                      finaltraj_filepath,
                      energies=final_energies)
        
    print('Result trajectory printed under', finaltraj_filepath)
            
# the following section contains all the callback functions for the NEB loop
# --------------------------------------------------------------------

# callback function used to predict the optimization steps
# for the images. It needs to work both with the analytical
# position scheme on or off.

def predict_AMGD_steps(NEBPath_obj, conf_dict):
    # first gather a bunch of data from the NEBPath
    # and the relevant settings from the conf dict
    nebgrads = NEBPath_obj.get_nebgrads()
    energies = NEBPath_obj.get_energies()
    img_dim = NEBPath_obj.n_img_dim()
    
    ci_index = get_current_CI_index(NEBPath_obj, conf_dict)
    
    # remove spring contribution from neb gradients
    # if analytical position scheme is active
    if conf_dict['use_analytical_springpos']:
        nebgrads = get_springless_nebgrads(NEBPath_obj, conf_dict)
                
    # now check if the AMGD step predictor objects have been
    # set up yet in the NEBPath misc. data. If not, this is
    # the first iteration, and they need to be newly created.
    if 'AMGD_objs' not in NEBPath_obj.misc_data:
        setup_AMGD_step_predictors(NEBPath_obj, conf_dict)
        
    # retrieve the AMGD step predictors for use
    AMGD_objs = NEBPath_obj.misc_data['AMGD_objs']
            
    # Perform the step prediction
    steps = []
    
    for i in range(len(AMGD_objs)):
        if energies[i] is None:
            # the engrad calculation for this image failed.
            # it will probably be reinterpolated/moved across
            # PES to some different region. It is best to
            # forget the data stored about it so far for this
            # image. Step vectors from this function will
            # be undone by the reinterpolation anyway
            AMGD_objs[i].reset()
            
            steps.append(np.zeros(img_dim))
            
        else:
            step = AMGD_objs[i].predict(nebgrads[i])
            
            steps.append(step)
            
    steps = np.array(steps)
    
    # If the user chose analytical position scheme,
    # we now add in the spring steps from the analytical
    # position scheme 
    if conf_dict['use_analytical_springpos']:
        full_path_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
        tanvecs = NEBPath_obj.get_tanvecs()
        img_pair_ks = NEBPath_obj.get_img_pair_ks()
        
        # we have to calculate the springsteps for the positions as they
        # will be with their optimization steps applied, otherwise
        # the analytical position scheme wont converge properly
        # full_path_pvecs[1:-1] += steps
        
        ap_spring_steps = sfm.calc_analytic_springsteps(full_path_pvecs,
                                                        img_pair_ks,
                                                        tanvecs,
                                                        ci_index)
                                                        
        # make sure none of the spring steps exceed max stepsize
        ap_spring_steps = spm.enforce_maxsteps(ap_spring_steps,
                                               conf_dict['max_step'])
                                                        
        # again, we will not compute steps for failed images,
        # so we zero the spring steps in question
        for i in range(len(ap_spring_steps)):
            if energies[i] is None:
                ap_spring_steps[i][:] = 0.0    
        
        # add spring steps to our optimization steps
        steps += ap_spring_steps
        
    # Apply atom freezing if selected by user
    if conf_dict['frozen_atom_indices'] is not None:
        # their nebgrads should already be zeroed
        # at this point. to ensure these atoms are not moved,
        # we zero their step vecs as well.
        
        # convert config entry into an actual python list,
        # then apply atom freezing to all step vectors
        frozen_index_list = parse_index_list(conf_dict['frozen_atom_indices'])
        
        for i in range(len(steps)):
            steps[i] = ngm.freeze_atom_indices(steps[i], frozen_index_list)
                   
    return steps

 
# helper functions for the step predictor function above
    
def setup_AMGD_step_predictors(NEBPath_obj, conf_dict):
    # generate the AMGD step predictors, then save them in the NEBPath

    AMGD_objs = [spm.AMGD(ndim=NEBPath_obj.n_img_dim(),
                          stepsize_fac=conf_dict['AMGD_stepsize_fac'],
                          max_gamma=conf_dict['AMGD_max_gamma'],
                          max_step=conf_dict['max_step'])
                 for i in range(NEBPath_obj.n_images())]
                 
    NEBPath_obj.misc_data['AMGD_objs'] = AMGD_objs
    
    
def get_springless_nebgrads(NEBPath_obj, conf_dict):
    tanvecs = NEBPath_obj.get_tanvecs()
    nebgrads = NEBPath_obj.get_nebgrads()
    
    ci_index = get_current_CI_index(NEBPath_obj, conf_dict)
    
    # project out the spring contribution, which is parallel to the tangent
    for i in range(len(nebgrads)):
        # except if there is a climbing image,
        # which has a special NEB gradient.
        if i != ci_index:
            nebgrads[i] = ngm.reject(nebgrads[i], tanvecs[i])
            
    return nebgrads
    
    
def get_current_CI_index(NEBPath_obj, conf_dict):
    # this function returns the NEBPath_obj's current
    # CI index if CI is currently active, or None
    # if CI is inactive
    if conf_dict['climbing_image'] is True:
        # update CI to be the highest energy image
        NEBPath_obj.update_ci_index()
    
        ci_index = NEBPath_obj.get_ci_index()
        
    else:
        ci_index = None
        
    return ci_index
                    
                    
# callback function used to calculate springforce gradients
# of the images, as well as to recalculate the variable k
# constants if variable k and CI are active.

def calc_springgrads(NEBPath_obj, conf_dict):
    if conf_dict['use_vark_ci'] and conf_dict['climbing_image']:
        # as of now, vark is only used in conjunction with CI.
        # update first the variable spring constants in the NEBPath.
        recalculate_ci_varks(NEBPath_obj, conf_dict)
        
    else:
        # if not, set all ks to be the value set in the conf_dict,
        # to make sure
        NEBPath_obj.set_img_k_const(conf_dict['k_const'])
        
    # calculate regular springforce gradients.
    # even if analytical positions scheme is active,
    # they are still needed for calculating convergence
    # thresholds.
    full_path_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
    img_pair_ks = NEBPath_obj.get_img_pair_ks()
    
    springgrads = sfm.simple_springgrads(full_path_pvecs, img_pair_ks)
    
    # if ci is active, the springforces acting on the ci are zero
    ci_index = get_current_CI_index(NEBPath_obj, conf_dict)
    
    if ci_index is not None:
        springgrads[ci_index][:] = 0.0
        
    return springgrads
 
 
# helper function for the spring gradient function above
    
def recalculate_ci_varks(NEBPath_obj, conf_dict):
    # So far, vark is only used in conjunction with CI,
    # so this assumes that the ci_index will not be none
    full_path_energies = NEBPath_obj.get_energies(include_ends=True)
    full_path_ci_index = get_current_CI_index(NEBPath_obj, conf_dict) + 1
    
    maxk = conf_dict['k_const']
    mink = conf_dict['k_const'] * conf_dict['vark_min_fac']
    
    pairwise_ks = sfm.compute_pairwise_ks(full_path_energies,
                                          full_path_ci_index,
                                          maxk,
                                          mink)
                                          
    NEBPath_obj.set_img_pair_ks(pairwise_ks)


# callback function for calculating neb gradients,
# after engrads, springgrads, tanvecs have all been
# calculated and saved in the NEBPath object.

def calc_nebgrads(NEBPath_obj, conf_dict):
    ci_index = get_current_CI_index(NEBPath_obj, conf_dict)

    raw_nebgrads = ngm.calculate_nebgrads(NEBPath_obj.get_engrads(),
                                          NEBPath_obj.get_springgrads(),
                                          NEBPath_obj.get_tanvecs(),
                                          ci_index)
                                          
    # project out translation and/or rotation from neb gradients
    # (experimental feature), if selected by user
    nebgrads = ngm.sanitize_stepvecs(raw_nebgrads,
                                     conf_dict['remove_gradtrans'],
                                     conf_dict['remove_gradrot'])
                                     
    # zero the gradients corresponding to frozen atoms,
    # so they don't skew the signals of convergence thresholds
    frozen_atom_indices = conf_dict['frozen_atom_indices']
    
    if frozen_atom_indices is not None:
        # convert config entry into an actual python list,
        # then apply atom freezing for each step vector
        frozen_index_list = parse_index_list(frozen_atom_indices)
        
        for i in range(len(nebgrads)):
            nebgrads[i] = ngm.freeze_atom_indices(nebgrads[i], frozen_index_list)
                                     
    return nebgrads


# callback function for computing image tangent vectors.
# we want it to be able to both return normal tangents,
# or apply smoothing for the tangent vectors, depending on
# what the user chose.

def calc_tanvecs(NEBPath_obj, conf_dict):
    # first, compute unsmoothed tans
    full_path_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
    full_path_energies = NEBPath_obj.get_energies(include_ends=True)
    
    # use henkelman-jonsson tangents
    raw_tans = tgm.henkjon_tans(full_path_pvecs,
                                full_path_energies)
                                             
    # if tangent smoothing is inactive, we are done
    if conf_dict['use_smooth_tans'] is False:
        return raw_tans
        
    else:
        # otherwise, we first must check if the tangent estimator
        # objects are set up already, and set them up if not
        if 'smoothed_tan_estimators' not in NEBPath_obj.misc_data:
            setup_smoothed_tan_estimators(NEBPath_obj, conf_dict)
            
        # retrieve tangent estimator objects for use
        smoothed_tan_estimators =\
            NEBPath_obj.misc_data['smoothed_tan_estimators']
            
        # perform tangent smoothing with the estimator objects
        img_energies = NEBPath_obj.get_energies()
        smoothed_tans = []
        
        for i in range(len(raw_tans)):
            # if an image is failed, it may end up being moved
            # quite a lot across the PES, so it makes sense to reset
            # its tangent estimation data, so it can catch up quicker
            if img_energies[i] is None:
                smoothed_tan_estimators[i].reset()
                
            # update tangent estimator with newest raw tangent
            smoothed_tan_estimators[i].update_tangent(raw_tans[i])
            
            # get tangent estimate
            smoothed_tan = smoothed_tan_estimators[i].get_tangent_estimate()
            
            smoothed_tans.append(smoothed_tan)
            
        return np.array(smoothed_tans)
 
 
# helper function for the tangent function above
    
def setup_smoothed_tan_estimators(NEBPath_obj, conf_dict):
    tansmooth_alpha = conf_dict['tansmooth_alpha']

    ste_objects = [tgm.SmoothedTangentEstimator(tansmooth_alpha)
                   for i in range(NEBPath_obj.n_images())]
                           
    NEBPath_obj.misc_data['smoothed_tan_estimators'] = ste_objects

   
# callback function that checks if NEB has reached an unrecoverable
# state and should be aborted


def giveup_signal_func(NEBPath_obj, steps, conf_dict):
    # the only giveup state is if there are too many failed images.
    # we check for that here.
    img_energies = NEBPath_obj.get_energies()
    
    failed_img_count = 0
    
    for energy in img_energies:
        if energy is None:
            failed_img_count += 1
            
    failure_percentage = failed_img_count / len(img_energies)
    
    if failure_percentage >= conf_dict['failed_img_tol_percent']:
        # signal that the NEB should be aborted
        print('Too many images failed to converge (' +
              str(failure_percentage * 100.0) + ' % failed).' +
              ' The NEB will be aborted. NOTE: the NEB ' +
              'did not converge! Be careful with the results!')
              
        return True
        
    else:
        # Signal that the NEB should not give up
        return False
        
        
# callback function for reinterpolating failed images
      
def failed_image_replacer_func(NEBPath_obj, conf_dict):                   
    full_path_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
    full_path_energies = NEBPath_obj.get_energies(include_ends=True)
    
    # our interpolation function is the same we used for generating
    # the initial path
    interp_func =  pim.interpolate_path
    
    # set up additional arguments needed by that function
    interp_func_kwargs = {'labels' : NEBPath_obj.get_labels(),
                          'interp_mode' : conf_dict['interp_mode'],
                          'rot_align_mode' : conf_dict['rot_align_mode'],
                          'do_IDPP_pass' : conf_dict['do_IDPP_pass']}
                          
    # perform the reinterpolation of failed images
    new_full_path_pvecs = fir.replace_failed_images(full_path_pvecs,
                                                    full_path_energies,
                                                    interp_func,
                                                    interp_func_kwargs)
                                                    
    
    # in addition to the image replacing, the images need to
    # be recentered and rotationally realigned every iteration.
    new_full_path_pvecs = pim.sanitize_path(new_full_path_pvecs,
                                            conf_dict['rot_align_mode'],
                                            do_IDPP_pass=False)
    
    # isolate image pvecs from the ends, which we will not replace
    patched_images = new_full_path_pvecs[1:-1]
    
    # replace NEBPath images with the now patched path
    NEBPath_obj.set_img_pvecs(patched_images)
    
    # return the edited path
    return NEBPath_obj
    
    
# the following section contains the callback functions for checking
# NEB convergence. There are several stages of optimization (sloppy NEB,
# NEB-CI, regular NEB) which have different convergence thresholds.
# This is implemented by having three slightly different checker functions.

def conv_checker_func_sloppy(NEBPath_obj, steps, conf_dict):             
    # first, if there are any failed images left, its not converged
    energies = NEBPath_obj.get_energies()
    
    if (energies == None).any():
        print('There are still failed images left in the path.' +
              ' There is no point in checking for signals of ' +
              'convergence yet.')
        
        return False
        
    # now check for signals of convergence. get the values from
    # conf dict
    
    Max_RMSF_tol = conf_dict['Sloppy_Max_RMSF_tol']
    Max_AbsF_tol = conf_dict['Sloppy_Max_AbsF_tol']
        
    # now perform the check for the signals of convergence
    nebgrads = get_equiv_nebgrads(NEBPath_obj, conf_dict)
    
    max_rmsf = csm.NEB_RMSF(nebgrads)
    max_absf = csm.NEB_ABSF(nebgrads)
    
    print('Current max. RMSF:', max_rmsf, 'Tol.', Max_RMSF_tol)
    print('Current max. AbsF:', max_absf, 'Tol.', Max_AbsF_tol)
    
    # print other information about the current NEB path
    do_iter_printout(NEBPath_obj, conf_dict)
    
    # compile rmsf, absf etc. and put them into the log
    values_dict = {'RMSF' : max_rmsf,
                   'AbsF' : max_absf}
                   
    logger = NEBPath_obj.misc_data['logger']
                   
    logger.write_to_log(NEBPath_obj, values_dict)
    
    # return the signal if converged
    if max_rmsf <= Max_RMSF_tol and max_absf <= Max_AbsF_tol:
        return True
        
    else:
        return False
        
        
def conv_checker_func_nosloppy_noci(NEBPath_obj, steps, conf_dict):             
    # first, if there are any failed images left, its not converged
    energies = NEBPath_obj.get_energies()
    
    if (energies == None).any():
        print('There are still failed images left in the path.' +
              ' There is no point in checking for signals of ' +
              'convergence yet.')
        
        return False
        
    # now check for signals of convergence. get the values from
    # conf dict
    
    Max_RMSF_tol = conf_dict['Max_RMSF_tol']
    Max_AbsF_tol = conf_dict['Max_AbsF_tol']
        
    # now perform the check for the signals of convergence
    nebgrads = get_equiv_nebgrads(NEBPath_obj, conf_dict)
    
    max_rmsf = csm.NEB_RMSF(nebgrads)
    max_absf = csm.NEB_ABSF(nebgrads)
    
    print('Current max. RMSF:', max_rmsf, 'Tol.', Max_RMSF_tol)
    print('Current max. AbsF:', max_absf, 'Tol.', Max_AbsF_tol)
    
    # print other information about the current NEB path
    do_iter_printout(NEBPath_obj, conf_dict)
    
    # compile rmsf, absf etc. and put them into the log
    values_dict = {'RMSF' : max_rmsf,
                   'AbsF' : max_absf}
                   
    logger = NEBPath_obj.misc_data['logger']
                   
    logger.write_to_log(NEBPath_obj, values_dict)
    
    # return the signal if converged
    if max_rmsf <= Max_RMSF_tol and max_absf <= Max_AbsF_tol:
        print('NEB convergence has been reached!')
    
        return True
        
    else:
        return False
        
        
def conv_checker_func_ci(NEBPath_obj, steps, conf_dict):
                      
    # first, if there are any failed images left, its not converged
    energies = NEBPath_obj.get_energies()
    
    if (energies == None).any():
        print('There are still failed images left in the path.' +
              ' There is no point in checking for signals of ' +
              'convergence yet.')
        
        return False
        
    # now check for signals of convergence. get the values from
    # conf dict
    Max_RMSF_tol = conf_dict['Max_RMSF_tol']
    Max_AbsF_tol = conf_dict['Max_AbsF_tol']
    CI_RMSF_tol = conf_dict['CI_RMSF_tol']
    CI_AbsF_tol = conf_dict['CI_AbsF_tol']
        
    # then, separate the nebgrad of the CI from the rest
    nebgrads = get_equiv_nebgrads(NEBPath_obj, conf_dict)
    ci_index = get_current_CI_index(NEBPath_obj, conf_dict)
    
    ci_nebgrad = nebgrads[ci_index]
    path_nebgrads = np.concatenate([nebgrads[:ci_index],
                                    nebgrads[ci_index+1:]])
        
    # now perform the check for the signals of convergence
    max_rmsf = csm.NEB_RMSF(path_nebgrads)
    max_absf = csm.NEB_ABSF(path_nebgrads)
    ci_rmsf = csm.RMS(ci_nebgrad)
    ci_absf = csm.MaxAbs(ci_nebgrad)
    
    print('Current max. RMSF:', max_rmsf, 'Tol.', Max_RMSF_tol)
    print('Current max. AbsF:', max_absf, 'Tol.', Max_AbsF_tol)
    print('Current CI RMSF:', ci_rmsf, 'Tol.', CI_RMSF_tol)
    print('Current CI AbsF:', ci_absf, 'Tol.', CI_AbsF_tol)
    
    # print other information about the current NEB path
    do_iter_printout(NEBPath_obj, conf_dict)
    
    # compile rmsf, absf etc. and put them into the log
    values_dict = {'RMSF' : max_rmsf,
                   'AbsF' : max_absf,
                   'CI_RMSF' : ci_rmsf,
                   'CI_AbsF' : ci_absf}
                   
    logger = NEBPath_obj.misc_data['logger']
                   
    logger.write_to_log(NEBPath_obj, values_dict)
    
    # return the signal if converged
    if (max_rmsf <= Max_RMSF_tol and
        max_absf <= Max_AbsF_tol and
        ci_rmsf <= CI_RMSF_tol and
        ci_absf <= CI_AbsF_tol):
        print('NEB convergence has been reached!')
    
        return True
        
    else:
        return False
        
        
# helper function for getting the nebgrads
# (or proper equivalent), depending on whether
# or not analytical spring positions are active
        
def get_equiv_nebgrads(NEBPath_obj, conf_dict):
    if conf_dict['use_analytical_springpos']:
        # equivalent nebgrads are orthogonal nebgrads
        # plus steps from the analytical pos scheme.
        orth_engrads = get_springless_nebgrads(NEBPath_obj, conf_dict)
        
        full_path_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
        tanvecs = NEBPath_obj.get_tanvecs()
        img_pair_ks = NEBPath_obj.get_img_pair_ks()
        ci_index = get_current_CI_index(NEBPath_obj, conf_dict)

        ap_spring_steps = sfm.calc_analytic_springsteps(full_path_pvecs,
                                                        img_pair_ks,
                                                        tanvecs,
                                                        ci_index)
                                                        
        equiv_nebgrads = orth_engrads + ap_spring_steps
        
        return equiv_nebgrads
        
    else:
        # if the analytical pos scheme is unused, just
        # return the regular nebgrads.
        return NEBPath_obj.get_nebgrads()


# helper function for printing some NEB stats to the console
        
def do_iter_printout(NEBPath_obj, conf_dict):
    path_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
    energies = NEBPath_obj.get_energies(include_ends=True)
    labels = NEBPath_obj.get_labels()
    
    io.write_xyz_traj(labels,
                      path_pvecs,
                      conf_dict['curtraj_path'],
                      energies=energies)
    
    max_energy = np.max(energies)
    
    left_barrier_kJmol = (max_energy - energies[0]) * Hartree_in_kJmol
    right_barrier_kJmol = (max_energy - energies[-1]) * Hartree_in_kJmol
    
    time_elapsed = time.time() - conf_dict['start_time']
    
    print('Wall time elapsed (h:m:s):', timedelta(seconds=time_elapsed))
    
    print('There are', NEBPath_obj.n_failed_images(),
          'failed images in the path.')
          
    ci_index = get_current_CI_index(NEBPath_obj, conf_dict)
        
    if ci_index is not None:
        print('Image at index', ci_index, 'is now the climbing image.')
    
    print('Approx. Barrier with respect to left end:',
          left_barrier_kJmol,
          'kJ/mol')
          
    print('Approx. Barrier with respect to right end:',
          right_barrier_kJmol,
          'kJ/mol')
 

# the following section contains the helper 
# function to gather the engrad function and its arguments,
# depending on which interface was chosen by the user.
# --------------------------------------------------------

def gather_engrfunc(atomic_labels, conf_dict, temp_workdir):
    interface = conf_dict['interface']
    
    if interface is None:
        # user forgot to set this
        raise nex.NEBError('Error: no interface for engrad calculation ' +\
                           'selected. Make sure to select an interface ' +\
                           'option in the input file.')
                           
    elif interface == 'orca':
        engrfunc, engrfunc_kwargs = setup_orca(atomic_labels,
                                               conf_dict,
                                               temp_workdir)
                                               
    elif interface == 'gaussian':
        engrfunc, engrfunc_kwargs = setup_gaussian(atomic_labels,
                                                   conf_dict,
                                                   temp_workdir)
                                                   
    elif interface == 'molpro':
        engrfunc, engrfunc_kwargs = setup_molpro(atomic_labels,
                                                 conf_dict,
                                                 temp_workdir)
                                                 
    elif interface == 'dummy':
        engrfunc, engrfunc_kwargs = setup_dummy()
        
    else:
        raise nex.NEBError('Error: invalid interface selected in input' +\
                           ' file: ' + str(interface))
                           
    return engrfunc, engrfunc_kwargs
    
    
# helper functions for setting up ORCA interface
    
def setup_orca(atomic_labels, conf_dict, temp_workdir):
    print('Orca interface for EnGrad calculation selected.')
    print('Temporary files stored at: ' + str(temp_workdir))
        
    engrfunc = orc.orca_path_engrads

    if conf_dict['method_line2'] is None:
        ml2 = None

    else:
        ml2 = codecs.decode(conf_dict['method_line2'], 'unicode_escape') 
        
    engrfunc_kwargs = {'labels' : atomic_labels,
                       'npal' : conf_dict['n_threads'],
                       'workingdir' : temp_workdir,
                       'orca' : find_orcapath(conf_dict),
                       'method_keywords' : conf_dict['method_keywords'],
                       'charge' : conf_dict['charge'],
                       'spin' : conf_dict['spin'],
                       'series_name' : 'image',
                       'method_line2' : ml2}
                       
    return engrfunc, engrfunc_kwargs
    
    
def find_orcapath(conf_dict):
    if conf_dict['orca_path'] is not None:
        print('Using orca path from .ini file.')

        orcapath = conf_dict['orca_path']

    else:
        orcapath = os.getenv('ORCA_EXE')

        if orcapath is None:
            raise nex.NEBError('ORCA_EXE environment variable not set. ' +\
                               'Set this variable before starting this ' +\
                               'program, or specify the "orca_path" ' +\
                               'keyword in the ini file.')
                                 
    return orcapath
    
    
# helper functions for setting up the Gaussian interface

def setup_gaussian(atomic_labels, conf_dict, temp_workdir):
    print('Gaussian interface for EnGrad calculation selected.')
    print('Temporary files stored at: ' + str(temp_workdir))
    print('Memory allocated: ' + str(conf_dict['memory']))
    
    engrfunc = gss.gaussian_path_engrads
    
    gauss_keywords = conf_dict['gaussian_keywords']
    
    if gauss_keywords is None:
        raise nex.NEBError('Error: gaussian interface selected, but no ' +\
                           'gaussian calculation keywords specified. Make' +\
                           ' sure to specify them in the .ini file under ' +\
                           'the "gaussian_keywords" keyword.')
    
    engrfunc_kwargs = {'labels' : atomic_labels,
                       'workingdir' : temp_workdir,
                       'series_name' : 'image',
                       'gaussian' : find_gausspath(conf_dict),
                       'method_keywords' : gauss_keywords,
                       'charge' : conf_dict['charge'],
                       'spin' : conf_dict['spin'],
                       'nprocs' : conf_dict['n_threads'],
                       'mem' : conf_dict['memory'],
                       'title' : 'Title Card Required'}
    
    return engrfunc, engrfunc_kwargs
    
    
def find_gausspath(conf_dict):
    if conf_dict['gaussian_path'] is not None:
        print('Using gaussian path from .ini file.')
                    
        gaussianpath = conf_dict['gaussian_path']
                    
    else:
        gaussianpath = os.getenv('GAUSS_EXE')
                    
        if gaussianpath is None:
            raise nex.NEBError('GAUSS_EXE environment variable not set. ' +\
                               'Set this variable before starting this ' +\
                               'program, or specify the "gaussian_path" ' +\
                               'keyword in the ini file.')

    return gaussianpath
    

# helper functions for setting up MolPro interface 
    
def setup_molpro(atomic_labels, conf_dict, temp_workdir):
    os.environ['TMPDIR'] = str(temp_workdir)
    os.environ['TMPDIR4'] = str(temp_workdir)
    
    print('Molpro interface for EnGrad calculation selected.')
    print('Temporary files stored at: ' + str(temp_workdir))
    print('Memory allocated: ' + str(conf_dict['memory']))
    
    engrfunc = mol.molpro_path_engrads
    
    molpro_keywords = conf_dict['molpro_keywords']
    
    if molpro_keywords is None:
        raise nex.NEBError('Error: molpro interface selected, but no ' +\
                           'molpro calculation keywords specified. Make' +\
                           ' sure to specify them in the .ini file under ' +\
                           'the "molpro_keywords" keyword.')
                         
    engrfunc_kwargs = {'labels' : atomic_labels,
                       'workingdir' : temp_workdir,
                       'series_name' : 'image',
                       'molpro' : find_molpro_path(conf_dict),
                       'method_keywords' : molpro_keywords,
                       'charge' : conf_dict['charge'],
                       'spin' : conf_dict['spin'],
                       'nprocs' : conf_dict['n_threads'],
                       'mem' : conf_dict['memory']}
                       #'errordir' : find_workdir()}
                       
    return engrfunc, engrfunc_kwargs
    
    
def find_molpro_path(conf_dict):
    if conf_dict['molpro_path'] is not None:
        print('Using molpro path from .ini file.')
                
        molpath = Path(conf_dict['molpro_path'])
                
    else:
        molpath = os.getenv('MOL_EXE')
                
        if molpath is None:
            raise nex.NEBError('MOL_EXE environment variable not set. Set ' +\
                               'this variable before starting this program' +\
                               ', or specify the "molpro_path" keyword in ' +\
                               'the ini file.')

        molpath = Path(molpath)
        
    return molpath
    
    
# helper function for setting up the dummy interface

def setup_dummy():
    engrfunc = dmm.dummy_path_engrads
    
    engrfunc_kwargs = {}
    
    return engrfunc, engrfunc_kwargs
    

# the following section contains helper functions for gathering starting
# trajectories or gathering end structures and interpolating starting paths
# --------------------------------------------------------------------------


def produce_starting_path(conf_dict):
    if conf_dict['starttraj'] is not None:
        # a starting trajectory was given, just gather that one
        labels, starting_path = gather_starting_path(conf_dict)
        
        # apply IDPP, structure alignment if selected
        starting_path = pim.sanitize_path(starting_path,
                                          conf_dict['rot_align_mode'],
                                          conf_dict['do_IDPP_pass'])
            
    else:
        # two end structures should be given, generate starting path from that
        if conf_dict['TS_guess'] is None:
            # only the two ends, gather them, make interpolation
            labels, starting_path = generate_from_ends(conf_dict)
            
        else:
            # a TS guess structure has been given, make interpolation
            # with that structure in the middle of the path
            labels, starting_path = generate_from_ends_and_TS(conf_dict)
            
    return labels, starting_path


def gather_end_structures(conf_dict):
    start_xyz = conf_dict['start_structure']
    end_xyz = conf_dict['end_structure']
    
    if start_xyz is None or end_xyz is None:
        raise nex.NEBError('start_structure and end_structure have not' +
                           ' both be set!')
    
    start_labels, start_pvec = io.read_xyz_file(start_xyz)
    end_labels, end_pvec = io.read_xyz_file(end_xyz)
    
    if start_labels != end_labels:
        raise nex.NEBError('Atomic labels for the two' +\
                       ' end structures do not match!')
                       
    labels = start_labels
    
    return labels, start_pvec, end_pvec
    
    
def gather_starting_path(conf_dict):
    starttraj = conf_dict['starttraj']
    
    labels, path_pvecs = io.read_xyz_traj(starttraj, dtype=Path)
    
    return labels, path_pvecs
    
    
def gather_TS_guess(conf_dict):
    tsguess_xyz = conf_dict['TS_guess']
    
    ts_labels, ts_pvec = io.read_xyz_file(tsguess_xyz)
    
    return ts_labels, ts_pvec
    
    
def generate_from_ends(conf_dict):
    labels, start_pvec, end_pvec = gather_end_structures(conf_dict)
    
    # recenter and align the given structures
    end_sequence = np.vstack([start_pvec, end_pvec])
    
    end_sequence = sam.align_path(end_sequence,
                                  conf_dict['rot_align_mode'])
                                  
    start_pvec = end_sequence[0]
    end_pvec = end_sequence[1]
    
    # generate the starting path 
    startpath = pim.interpolate_path(start_pvec,
                                     end_pvec,
                                     conf_dict['n_images'],
                                     labels,
                                     conf_dict['interp_mode'],
                                     conf_dict['rot_align_mode'],
                                     conf_dict['do_IDPP_pass'])
                                     
    return labels, startpath
    
    
def generate_from_ends_and_TS(conf_dict):
    labels, start_pvec, end_pvec = gather_end_structures(conf_dict)
    ts_labels, ts_pvec = gather_TS_guess(conf_dict)
    
    if labels != ts_labels:
        raise nex.NEBError('Atomic labels of end structures and TS guess' +
                           ' do not match!')
                           
    # recenter and align the given structures
    end_sequence = np.vstack([start_pvec, ts_pvec, end_pvec])
    
    end_sequence = sam.align_path(end_sequence,
                                  conf_dict['rot_align_mode'])
                                  
    start_pvec = end_sequence[0]
    ts_pvec = end_sequence[1]
    end_pvec = end_sequence[2]
                        
    # generate the starting path                     
    n_imgs_per_segment= int(conf_dict['n_images'] / 2)
                           
    segment1 = pim.interpolate_path(start_pvec,
                                    ts_pvec,
                                    n_imgs_per_segment,
                                    labels,
                                    conf_dict['interp_mode'],
                                    conf_dict['rot_align_mode'],
                                    conf_dict['do_IDPP_pass'])
                                    
    segment2 = pim.interpolate_path(start_pvec,
                                    ts_pvec,
                                    n_imgs_per_segment,
                                    labels,
                                    conf_dict['interp_mode'],
                                    conf_dict['rot_align_mode'],
                                    conf_dict['do_IDPP_pass'])
                                    
    complete_path = np.concatenate([segment1, segment2[1:]])
    
    return labels, complete_path
    
    
def setup_NEBPath_obj(labels, starting_path, engrfunc, engrfunc_kwargs, conf_dict):
    # with the engrad function, we can calculate the energies of the ends
    # for the nebpath object
    end_pvecs = np.array([starting_path[0], starting_path[-1]])
    
    end_energies, end_engrads = engrfunc(end_pvecs, **engrfunc_kwargs)
    
    if end_energies[0] is None or end_energies[1] is None:
        # If the ends fail to converge, that usually means
        # there's something wrong with the interface or the
        # Engrad calculation method that the user set up
        raise nex.EngradError('Error: One or more of the end structures ' +
                              'failed to converge. Check the end structures' +
                              ' theory level, and other calculation aspects.')
    
    # generate nebpath object
    nebpath = nopt.NEBPath(start_pvec=starting_path[0],
                           end_pvec=starting_path[-1],
                           start_energy=end_energies[0],
                           end_energy=end_energies[-1],
                           img_pvecs=starting_path[1:-1],
                           labels=labels,
                           initial_k=conf_dict['k_const'])
                           
    return nebpath
    
    
# the following section contains helper functions for processing
# the arguments given to the program upon being started from
# console, as well as helper functions for finding/generating
# the result directory and the directory for temporary files.
# -------------------------------------------------------------

    
def process_input_path(inpfile_path):
    # check if inpfile_path is an actual file,
    # or if it's a directory, which would mean
    # its a job using the old workdir system
    
    if os.path.isfile(inpfile_path):
        # load the input file normally
        conf_dict = conf.load_inputfile(inpfile_path)
        
    elif os.path.isdir(inpfile_path):
        # apparently, we have an old-style workdir.
        # attempt to process it as such
        print('Warning: directory was given as argument in program call.' +
              ' Attempting to process as an old-style workdir. For the ' +
              'new style of calling this program, refer to the manual.')
              
        conf_dict = process_workdir(inpfile_path)
        
    else:
        raise nex.NEBError('Error: argument given in program call is no '
                           'valid file or directory: ' + str(inpfile_path))
                           
    return conf_dict
    
    
def process_moreargs(conf_dict, workdir, arguments):
    if '-trajtest' in arguments:
        conf_dict['trajtest'] = True
        # we are not going to use tempdir anyway
        conf_dict['tempdir'] = workdir
        
    return conf_dict


def process_workdir(wdirpath):
    contents = list(os.listdir(wdirpath))
    
    # try to find ini file
    inifiles = [item for item in contents if item.endswith('.ini')]
    
    if len(inifiles) != 1:
        raise nex.NEBError('Error: ' + str(wdirpath) + ' is not a valid' +
                           ' workdir. It must contain exactly one ini file.')
                           
    inipath = wdirpath / inifiles[0]
    
    # try to read the ini, skipping the '[options]' line at the start
    conf_dict = conf.load_inputfile(inipath)
    
    # if we have a starttraj given, the conf data is already compatible
    if conf_dict['starttraj'] is not None:
        return conf_dict
        
    else:
        # the system for providing TS guesses is also the same to the old.
        # only the end structure filepaths would need to be added.
        # try to find them now.
        xyz_filepaths = [wdirpath / name for name in contents
                         if name.endswith('.xyz')]
        
        # remove xyz files already accounted for
        if conf_dict['TS_guess'] is not None:
            TS_path = Path(conf_dict['TS_guess'])
        
            xyz_filepaths = [filepath for filepath in xyz_filepaths
                             if not os.path.samefile(filepath, TS_path)]
                             
        # we should be left with exactly two xyz files
        if len(xyz_filepaths) != 2:
            raise nex.NEBError('Error: ' + str(wdirpath) + ' is not a valid' +
                               ' workdir. There must be two xyz files for ' +
                               'the two end structures.')
                               
        # put in the missing data to make conf compatible,
        # then return
        conf_dict['start_structure'] = str(xyz_filepaths[0])
        conf_dict['end_structure'] = str(xyz_filepaths[1])
        conf_dict['jobname'] = wdirpath.parts[-1]
        
        return conf_dict
    
    
def find_workdir(conf_dict, inpfile_path):
    if conf_dict['jobdir'] is None:
        workdir = inpfile_path.parent / conf_dict['jobname']
        
    else:
        workdir = Path(conf_dict['jobdir'])
        
    # make sure to get the full path
    workdir = get_full_path(workdir)
        
    # in this directory, create a results folder
    folder_name = find_resultfolder_name(workdir)
    
    workdir = workdir / folder_name
        
    # check if this directory can be created and accessed properly
    try:
        io.safe_create_dir(workdir)
        
    except nex.NEBError:
        print('Error when trying to access job directory.' +
              ' See message below. Make sure the jobdir ' +
              'setting in the input file is set as intended.')
              
        raise
        
    # the jobdir was successfully found and/or created.
    return workdir
    

def find_resultfolder_name(target_dir):
    # first, find any existing results folders
    if not os.path.isdir(target_dir / 'results'):
        return 'results'
    
    folder_nr = 0
    
    while True:
        folder_nr += 1
        
        folder_name = 'results' + str(folder_nr)
        
        if not os.path.exists(target_dir / folder_name):
            return folder_name
    
    
def find_tempdir(conf_dict):
    if conf_dict['tempdir'] is not None:
        print('Using tempdir specified in input file.')
        
        tempdir = Path(conf_dict['tempdir'])
        
    else:
        tempdir = os.getenv('TMPDIR')
        
        if tempdir is None:
            raise ValueError('Error: TMPDIR environment variable not set.' +\
                             'Set this variable before starting, or ' +\
                             'specify a directory under the "tempdir" ' +\
                             'keyword in the input file.')
            
        tempdir = Path(tempdir)
        
    # make sure to get the full path
    tempdir = get_full_path(tempdir)
        
    # check if this directory can be accessed properly
    if not io.check_if_directory(tempdir):
        raise nex.NEBError('Error when trying to access temporary' +
                           ' files directory. See message above.' +
                           ' Make sure you set the tempdir setting ' +
                           'in the input file, or the TMPDIR environment' +
                           ' variable correctly.')
    
    # generate a folder in the directory for our job
    temp_workdir = tempdir / generate_tempdirname(conf_dict['jobname'])
    
    io.safe_create_dir(temp_workdir)
    
    print('Temporary files stored under: ' + str(temp_workdir))
    
    return temp_workdir
    
    
# miscellaneous helper functions
# -----------------------------


def get_full_path(relpath):
    abspath = os.path.abspath(os.path.expanduser(os.path.expandvars(relpath)))
    
    return Path(abspath)


def parse_index_list(index_list_conf_entry):
    if index_list_conf_entry is None:
        return []
        
    else:
        # convert the list string representation
        # into a list of ints
        return [int(token) for token in
                reverse_bettersplit(index_list_conf_entry)]
                

def reverse_bettersplit(text, not_delims='0123456789'):
    tokens = []
    token = ''
    
    for char in text:
        if char in not_delims:
            token += char
        
        elif token != '':
            tokens.append(token)
            token = ''
            
    if token != '':
        tokens.append(token)
        
    return tokens
    
    
def generate_tempdirname(jobname):
    numerals = '0123456789'
    name = jobname + '_'
    
    # sleep waits 1 second only approximately.
    # by doing this, we ensure that multiple
    # jobs started simultaneously without a
    # jobname will not have the exact same
    # folder name inside the tempdir
    time.sleep(1)
    
    dtime = str(datetime.now())
    
    for char in dtime:
        if char in numerals:
            name += char
            
    return name
    
    

if __name__ == '__main__':
    main()
