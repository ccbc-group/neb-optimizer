import numpy as np
import sys
from neb_optimizer import do_opt_loop, NEBPath
from step_pred_module import AMGD
import springforce_module as sfm
import tangent_module as tgm
import nebgrad_module as ngm
import convsig_module as cm
import struct_aligner_module as sam

import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")


# helper functions for calculating IDPP gradients
# -------------------------------------------------


def pvec_to_crdmat(pvec):
    natoms = int(len(pvec) / 3)
    
    return pvec.reshape((natoms, 3))

   
def crdmat_to_pvec(crdmat):
    return crdmat.flatten()


def calculate_distmat(pvec):
    crdmat = pvec_to_crdmat(pvec)
    
    natoms = len(crdmat)
    
    distmat = np.zeros((natoms, natoms))
    
    for i in range(natoms):
        for j in range(i+1, natoms):
            dist = np.linalg.norm(crdmat[i] - crdmat[j])
            
            distmat[i, j] = dist
            distmat[j, i] = dist
            
    return distmat
    
    
# Functions for calculating the IDPP S value (stand-in for energy),
# and its gradient, used for structure preoptimization in IDPP.
# ------------------------------------------------------------------


def IDPP_S(pvec, ideal_dmat, w_exp=4.0):
    real_dmat = calculate_distmat(pvec)
    
    omega = np.power(ideal_dmat, -w_exp)
    
    for i in range(omega.shape[0]):
        omega[i][i] = 0.0
        
    delta_d = ideal_dmat - real_dmat
    
    s_vals = omega.flatten() * delta_d.flatten()**2
    
    return np.sum(s_vals) * 0.5


def IDPP_gradS(pvec, ideal_dmat, w_exp=4.0):
    dmat = calculate_distmat(pvec)
    
    coords = pvec_to_crdmat(pvec)
    
    natoms = len(coords)
    
    gradient = []
            
    for i in range(natoms):
        # analytic expression for the gradient of the
        # IDPP S-value
        
        real_d = dmat[i]
        ideal_d = ideal_dmat[i]
        
        deltapart = -(coords - coords[i])
        
        diffpart = ideal_d / real_d
        diffpart[i] = 1.0
        diffpart -= 1.0
        
        facpart = np.power(ideal_d, -w_exp)
        facpart[i] = 1.0
        
        vecpart = diffpart * facpart
        vecpart = vecpart.reshape(len(vecpart), 1)
        
        xyz_grads = -deltapart * vecpart
        
        # gradient for the x, y, z coords of one atom
        # this is calculated for every atom
        xyz_grad = 2.0 * np.sum(xyz_grads, axis=0)
        
        gradient.append(xyz_grad)
            
    return np.array(gradient).flatten()
    
    
# miscellaneous helper functions
# ----------------------------------------------------------------------
def interpolate_linear(startvec, stopvec, n_interps):
    # n_interps is the number of new images to generate
    # in between the ends provided in the function arguments.
    interp_facs = np.linspace(0.0, 1.0, num=n_interps+2)

    interp_vecs = [startvec + (stopvec - startvec) * fac
                   for fac in interp_facs]
                   
    return np.array(interp_vecs)
    
    
# The following is a stand-alone implementation of an IDPP preoptimizer
# function for NEB starting paths. It uses code and step predictors
# from other modules of the program to perform its own NEB optimization
# using the IDPP gradient, as described in the paper introducing IDPP.
# [Insert Reference Here]
    
# callback functions for the NEB optimization using the IDPP gradients
# (which is what an IDPP optimization is)
# ----------------------------------------------------------------------


def calc_IDPP_engrads(img_pvecs, ideal_dmats, w_exp=4.0):
    # expects len(ideal_dmats) == len(img_pvecs)-2
    
    gradvecs = [IDPP_gradS(pvec, ideal_dmat, w_exp=w_exp)
                for pvec, ideal_dmat in zip(img_pvecs, ideal_dmats)]
                
    energies = [IDPP_S(pvec, ideal_dmat, w_exp=w_exp)
                for pvec, ideal_dmat in zip(img_pvecs, ideal_dmats)]
                
    return np.array(energies), np.array(gradvecs)


def calc_IDPP_springgrads(NEBPath_obj, img_pair_ks):

    all_img_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
    
    return sfm.simple_springgrads(all_img_pvecs, img_pair_ks)
    
    
def calc_IDPP_tanvecs(NEBPath_obj):

    all_img_pvecs = NEBPath_obj.get_img_pvecs(include_ends=True)
    
    return tgm.simple_tans(all_img_pvecs)
    
    
def calc_IDPP_nebgrads(NEBPath_obj):
    engrads = NEBPath_obj.get_engrads()
    sprgrads = NEBPath_obj.get_springgrads()
    tanvecs = NEBPath_obj.get_tanvecs()
    
    nebgrads = ngm.calculate_nebgrads(engrads,
                                      sprgrads,
                                      tanvecs)
                                
    # project out translations to make sure
    return ngm.sanitize_stepvecs(nebgrads,
                                 reject_transl=True,
                                 reject_rot=False)
    

def predict_IDPP_optsteps(NEBPath_obj, stepsize):
    # there should be no failed images in IDPP,
    # so we do not need to take any precautions
    # for the case of empty nebgrads of failed images
    nebgrads = NEBPath_obj.get_nebgrads()
    
    """
    steps = []
    
    for i in range(len(AMGD_objs)):
        steps.append(AMGD_objs[i].predict(nebgrads[i]))
        
    return np.array(steps)
    """
    
    # for some reason, it seems gradient descent
    # performs better
    return -stepsize * nebgrads
    
    
def IDPP_conv_thresh_func(NEBPath_obj,
                          opt_steps,
                          RMSF_thresh,
                          MAXF_thresh):
                          
    IDPP_neb_grads = NEBPath_obj.get_nebgrads()
    
    rmsf = cm.NEB_RMSF(IDPP_neb_grads)
    
    absf = cm.NEB_ABSF(IDPP_neb_grads)
    
    print('IDPP Max. RMSF:', rmsf, 'Tol.:', RMSF_thresh)
    print('IDPP Max. Abs. F:', absf, 'Tol.:', MAXF_thresh)
    
    if rmsf > RMSF_thresh:
        return False
    
    if absf > MAXF_thresh:
        return False
        
    return True
    
                                    
def IDPP_giveup_signal_func(NEBPath_obj, opt_steps):
    # there should be no unrecoverable state in IDPP,
    # so we just always signal False (no giving up).
    
    return False
    
    
def IDPP_failed_image_replacement_func(NEBPath_obj):
    # there should be no failed images in IDPP,
    # so we will not replace any images. However,
    # the images still need to be realigned every
    # iteration.
    
    return NEBPath_obj
    
    
# The following is the standalone function for optimizing NEB starting paths,
# using the code from neb_optimizer module, and the callback functions above.
# ----------------------------------------------------------------------

# one must be careful to set the IDPP convergence thresholds not too tight.

def do_IDPP_opt_pass(path_pvecs,
                     rot_align_mode='full',
                     w_exp=4.0,
                     maxiter=1000,
                     RMSF_thresh=0.00945,
                     MAXF_thresh=0.0189,
                     stepsize=0.1):
                     
    # calculate 'ideal' distance matrices
    start_dmat = calculate_distmat(path_pvecs[0])
    stop_dmat = calculate_distmat(path_pvecs[-1])
    
    # we want len(path_pvecs)-2 interpolations,
    # because path_pvecs contains the ends,
    # which arent counted in interpolate_linear
    ideal_dmats = interpolate_linear(start_dmat,
                                     stop_dmat,
                                     len(path_pvecs)-2)
    
    # set up NEBPath object for the neb optimizer function
    
    # separate distmats of the ends from the rest
    # of the path
    start_dmat = ideal_dmats[0]
    img_dmats = ideal_dmats[1:-1]
    end_dmat = ideal_dmats[-1]
    
    # separate start and end pvecs from the rest of the images
    start_pvec = path_pvecs[0]
    img_pvecs = path_pvecs[1:-1]
    end_pvec = path_pvecs[-1]
    
    # here the 'energies' are probably just a formality,
    # but NEBPath objects should contain the energies
    # for all images
    start_energy = IDPP_S(start_pvec, start_dmat, w_exp=w_exp)
    end_energy = IDPP_S(end_pvec, end_dmat, w_exp=w_exp)
    
    # atomic labels dont matter in IDPP
    labels = []
    
    # Now, we have collected all data needed to generate
    # the NEBPath object that can be fed into the NEB optimizer
    # loop function from the neb_optimizer module.
    
    NEBPath_obj = NEBPath(start_pvec,
                          end_pvec,
                          start_energy,
                          end_energy,
                          img_pvecs,
                          labels,
                          initial_k=1.0)
                          
    # now set up the callback functions for the neb
    # optimizer loop, and their additional argument
    # dictionaries, if they need them.
    engrad_calc_func = calc_IDPP_engrads                            
    engrad_calc_kwargs = {'ideal_dmats' : img_dmats,
                          'w_exp' : w_exp}
    
    sprgrad_calc_func = calc_IDPP_springgrads
    sprgrad_calc_kwargs = {'img_pair_ks' : np.ones(len(path_pvecs)-1)}
    
    tanvec_calc_func = calc_IDPP_tanvecs
    
    nebgrad_calc_func = calc_IDPP_nebgrads
    
    step_pred_func = predict_IDPP_optsteps
    step_pred_kwargs = {'stepsize' : stepsize}
    
    conv_signal_func = IDPP_conv_thresh_func
    conv_signal_kwargs = {'RMSF_thresh' : RMSF_thresh,
                          'MAXF_thresh' : MAXF_thresh}
                          
    giveup_signal_func = IDPP_giveup_signal_func
    
    failed_img_repl_func = IDPP_failed_image_replacement_func
                          
    # feed all the stuff collected so far into the NEB optimizer function
    # to perform the IDPP optimization

    opt_NEBPath, ret_state, end_iter =\
        do_opt_loop(NEBPath_obj,
                    engrad_calc_func=engrad_calc_func,
                    sprgrad_calc_func=sprgrad_calc_func,
                    tanvec_calc_func=tanvec_calc_func,
                    nebgrad_calc_func=nebgrad_calc_func,
                    step_pred_func=step_pred_func,
                    conv_signal_func=conv_signal_func,
                    giveup_signal_func=giveup_signal_func,
                    failed_img_repl_func=failed_img_repl_func,
                    maxiter=maxiter,
                    engrad_calc_kwargs=engrad_calc_kwargs,
                    sprgrad_calc_kwargs=sprgrad_calc_kwargs,
                    step_pred_kwargs=step_pred_kwargs,
                    conv_signal_kwargs=conv_signal_kwargs,
                    silent_mode=True)
                    
    if ret_state == 'FAILED':
        print('Warning: IDPP did not converge!' +
              ' Be careful with the results! ' +
              'Study the messages above to find the cause.')
              
    else:
        print('IDPP pass is converged!')
        
    # recover the optimized geometries from the NEBPath object,
    # in the form of the 1D position vectors, and return them,
    # as well as the signal indicating if the IDPP converged
              
    opt_path_pvecs = opt_NEBPath.get_img_pvecs(include_ends=True)
                                 
    return opt_path_pvecs
